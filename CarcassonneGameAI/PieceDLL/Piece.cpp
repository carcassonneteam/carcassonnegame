#include "Piece.h"

Piece::Piece() :
	m_pieceZone(Piece::PieceZone::None)
{
	//Empty
}

Piece::Piece(PieceZone pieceZone):
	m_pieceZone(pieceZone)
{
	//Empty
}

Piece::PieceZone Piece::getPieceZone() const noexcept
{
	return m_pieceZone;
}

void Piece::setPieceZone(PieceZone pieceZone) noexcept
{
	this->m_pieceZone = pieceZone;
}
