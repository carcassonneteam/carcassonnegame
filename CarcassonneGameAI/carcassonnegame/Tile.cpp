#include "Tile.h"



Tile::Tile() :
	Tile(
		Side::None,
		Side::None,
		Side::None,
		Side::None,
		Side::None,
		Side::None,
		Side::None,
		Side::None,
		Center::None,
		BonusPoint::None
	)
{
	//Default Constructor
	//Empty
}

Tile::Tile(
	const Side a_top,
	const Side a_right,
	const Side a_bottom,
	const Side a_left,
	const Side a_topRight,
	const Side a_topLeft,
	const Side a_bottomRight,
	const Side a_bottomLeft,
	const Center a_center
) : Tile(
	a_top,
	a_right,
	a_bottom,
	a_left,
	a_topRight,
	a_topLeft,
	a_bottomRight,
	a_bottomLeft,
	a_center,
	BonusPoint::None)
{
	//Constructor for generating tiles
	//Empty
}

Tile::Tile(
	const Side a_top,
	const Side a_right,
	const Side a_bottom,
	const Side a_left,
	const Side a_topRight,
	const Side a_topLeft,
	const Side a_bottomRight,
	const Side a_bottomLeft,
	const Center a_center,
	const BonusPoint a_bonusPoint
) :
	m_top(a_top),
	m_right(a_right),
	m_left(a_left),
	m_bottom(a_bottom),
	m_center(a_center),
	m_bonus(a_bonusPoint),
	m_topLeft(a_topLeft),
	m_topRight(a_topRight),
	m_bottomLeft(a_bottomLeft),
	m_bottomRight(a_bottomRight)
{
	//Constructor for special pieces
	//Empty
}

Tile::~Tile()
{
	m_top = Side::None;
	m_topLeft = Side::None;
	m_topRight = Side::None;
	m_right = Side::None;
	m_left = Side::None;
	m_bottomLeft = Side::None;
	m_bottomRight = Side::None;
	m_bottom = Side::None;
	m_center = Center::None;
	m_bonus = BonusPoint::None;
}

Tile::Tile(const Tile& a_tile)
{
	*this = a_tile;
}

Tile::Tile(Tile&& a_tile)
{
	*this = std::move(a_tile);
}

auto Tile::operator= (const Tile& a_tile) -> Tile&
{
	m_top = a_tile.m_top;
	m_right = a_tile.m_right;
	m_left = a_tile.m_left;
	m_bottom = a_tile.m_bottom;
	m_center = a_tile.m_center;
	m_topLeft = a_tile.m_topLeft;
	m_topRight = a_tile.m_topRight;
	m_bottomLeft = a_tile.m_bottomLeft;
	m_bottomRight = a_tile.m_bottomRight;
	m_bonus = a_tile.m_bonus;

	return *this;
}

auto Tile::operator= (Tile&& a_tile) -> Tile&
{
	m_top = a_tile.m_top;
	m_right = a_tile.m_right;
	m_left = a_tile.m_left;
	m_bottom = a_tile.m_bottom;
	m_center = a_tile.m_center;
	m_topLeft = a_tile.m_topLeft;
	m_topRight = a_tile.m_topRight;
	m_bottomLeft = a_tile.m_bottomLeft;
	m_bottomRight = a_tile.m_bottomRight;
	m_bonus = a_tile.m_bonus;

	new(&a_tile) Tile;

	return *this;
}

auto Tile::getTop() const noexcept -> Tile::Side
{
	return this->m_top;
}

auto Tile::getTopLeft() const noexcept -> Side
{
	return this->m_topLeft;
}

auto Tile::getTopRight() const noexcept -> Side
{
	return this->m_topRight;
}

auto Tile::getRight() const noexcept -> Tile::Side
{
	return this->m_right;
}

auto Tile::getLeft() const noexcept -> Tile::Side
{
	return this->m_left;
}

auto Tile::getBottom() const noexcept -> Tile::Side
{
	return this->m_bottom;
}

auto Tile::getBottomLeft() const noexcept -> Side
{
	return this->m_bottomLeft;
}

auto Tile::getBottomRight() const noexcept -> Side
{
	return this->m_bottomRight;
}

auto Tile::getCenter() const noexcept -> Tile::Center
{
	return this->m_center;
}

auto Tile::getBonusPoint() const noexcept -> Tile::BonusPoint
{
	return this->m_bonus;
}

auto Tile::getPiece() noexcept -> Piece
{
	return this->m_pieceOfTile.value();
}

auto Tile::getSideFromZone(const Piece::PieceZone a_zone) const noexcept -> Side
{
	switch (a_zone)
	{
	case Piece::PieceZone::TopLeft:
		return getTopLeft();
	case Piece::PieceZone::Top:
		return getTop();
	case Piece::PieceZone::TopRight:
		return getTopRight();
	case Piece::PieceZone::Right:
		return getRight();
	case Piece::PieceZone::BottomRight:
		return getBottomRight();
	case Piece::PieceZone::Bottom:
		return getBottom();
	case Piece::PieceZone::BottomLeft:
		return getBottomLeft();
	case Piece::PieceZone::Left:
		return getLeft();
	default:
		return Side::None;
	}
}

auto Tile::getCenterFromZone(const Piece::PieceZone a_zone) const noexcept -> Center
{
	return getCenter();
}

auto Tile::setTop(const Side a_top) noexcept -> void
{
	this->m_top = a_top;
}

auto Tile::setTopLeft(const Side a_topLeft) noexcept -> void
{
	this->m_topLeft = a_topLeft;
}

auto Tile::setTopRight(const Side a_topRight) noexcept -> void
{
	this->m_topRight = a_topRight;
}

auto Tile::setRight(const Side a_right) noexcept -> void
{
	this->m_right = a_right;
}

auto Tile::setLeft(const Side a_left) noexcept -> void
{
	this->m_left = a_left;
}

auto Tile::setbottom(const Side a_bottom) noexcept -> void
{
	this->m_bottom = a_bottom;
}

auto Tile::setbottomLeft(const Side a_bottomLeft) noexcept -> void
{
	this->m_bottomLeft = a_bottomLeft;
}

auto Tile::setbottomRight(const Side a_bottomRight) noexcept -> void
{
	this->m_bottomRight = a_bottomRight;
}

auto Tile::setCenter(const Center a_center) noexcept -> void
{
	this->m_center = a_center;
}

auto Tile::setBonusPoint(const BonusPoint a_bonusPoint) noexcept -> void
{
	this->m_bonus = a_bonusPoint;
}

auto Tile::setPiece(const Piece a_piece) noexcept -> void
{
	this->m_pieceOfTile = a_piece;
}

auto Tile::rotateTileToRight() noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Tile rotated...", Logger::Level::Info);
	Tile l_auxTile = *this;
	this->setTopLeft(l_auxTile.getBottomLeft());
	this->setTop(l_auxTile.getLeft());
	this->setTopRight(l_auxTile.getTopLeft());

	this->setRight(l_auxTile.getTop());

	this->setbottomRight(l_auxTile.getTopRight());
	this->setbottom(l_auxTile.getRight());
	this->setbottomLeft(l_auxTile.getBottomRight());

	this->setLeft(l_auxTile.getBottom());
	of.close();
}

auto Tile::hasPiece() noexcept -> bool
{
	if (this->m_pieceOfTile.has_value())
		return true;
	return false;
}

auto Tile::placePiece(const Piece & a_piece) noexcept -> void
{
	this->m_pieceOfTile.emplace(a_piece.getPieceZone());
}
