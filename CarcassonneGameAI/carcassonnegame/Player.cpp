#include "Player.h"

Player::Player(const std::string& a_playerName) :
	m_nrOfPiecesAvailable(sc_maxNrOfPieces),
	m_playerName(a_playerName),
	m_score(0)
{
	//Empty
}

Player::Player(const Player & a_player)
{
	*this = a_player;
}

Player::Player(Player && a_player)
{
	*this = std::move(a_player);
}

auto Player::getPlayerName() const noexcept -> const std::string &
{
	return m_playerName;
}

auto Player::getNrOfPiecesAvailable() const noexcept -> int
{
	return m_nrOfPiecesAvailable;
}

auto Player::getScore() const noexcept -> int
{
	return m_score;
}

auto Player::getPlacedPieces() const noexcept-> const std::vector<std::pair<uint8_t, uint8_t>> &
{
	return m_placedPiecesOnBoard;
}

auto Player::setPlayerName(const std::string & a_name) noexcept -> void
{
	m_playerName = a_name;
}

auto Player::addScore(const int a_score) -> void
{
	m_score += a_score;
}

auto Player::removePiecesFromBoard(std::vector<Board::Position>& a_positions) -> void
{
	for (auto piece : a_positions)
	{
		auto it = std::find(m_placedPiecesOnBoard.begin(), m_placedPiecesOnBoard.end(), piece);
		if (it != m_placedPiecesOnBoard.end())
		{
			m_placedPiecesOnBoard.erase(it);
			m_nrOfPiecesAvailable++;
		}
	}
}

auto Player::addPieceOnTile(Piece & a_piece, std::pair<Tile, std::string> & a_tile, Piece::PieceZone a_pieceZone, Board::Position & a_tilePosition) -> void
{
	if (m_nrOfPiecesAvailable <= 0)
		throw std::exception("No more pieces!\n");
	m_nrOfPiecesAvailable--;
	a_tile.first.setPiece(a_piece);
	a_piece.setPieceZone(a_pieceZone);
	m_placedPiecesOnBoard.push_back(a_tilePosition);
}

auto Player::addTileOnBoard(std::pair<Tile, std::string>&& a_tile, Board::Position & a_position, Board & a_board) -> void
{
	PlaceTile::placeTile(a_position.first, a_position.second, std::move(a_tile), a_board);
}

auto Player::operator=(const Player & a_player) -> Player &
{
	m_nrOfPiecesAvailable = a_player.m_nrOfPiecesAvailable;
	m_playerName = a_player.m_playerName;
	m_score = a_player.m_score;
	m_placedPiecesOnBoard.clear();
	for (auto & piece : a_player.m_placedPiecesOnBoard)
		m_placedPiecesOnBoard.push_back(piece);
	return *this;
}

auto Player::operator=(Player && a_player) -> Player &
{
	m_nrOfPiecesAvailable = a_player.m_nrOfPiecesAvailable;
	m_playerName = a_player.m_playerName;
	m_score = a_player.m_score;
	m_placedPiecesOnBoard.clear();
	for (auto & piece : a_player.m_placedPiecesOnBoard)
		m_placedPiecesOnBoard.push_back(piece);

	new(&a_player) Player;

	return *this;
}

auto Player::operator==(const Player & a_otherPlayer) -> bool
{
	return m_playerName == a_otherPlayer.getPlayerName()
		&& m_nrOfPiecesAvailable == a_otherPlayer.getNrOfPiecesAvailable()
		&& m_score == a_otherPlayer.getScore()
		&& std::equal(m_placedPiecesOnBoard.begin(), m_placedPiecesOnBoard.end(), a_otherPlayer.getPlacedPieces().begin());

}