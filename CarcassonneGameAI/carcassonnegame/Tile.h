#pragma once

#include "../Logging/Logging.h"
#include "../PieceDLL/Piece.h"

#include <cstdint>
#include <optional>
/*
Tile class.
Holds the properties of object tile.
*/
class Tile
{
public:

	//enum for all the posible sides
	enum class Side : uint8_t
	{
		City,
		Road,
		Filed,
		None
	};

	//enum for center
	enum class Center : uint8_t
	{
		None,
		Intersection,
		Cloister,
		City
	};

	//Special enum for bonus points
	enum class BonusPoint : uint8_t
	{
		True,
		None
	};

public:
	//constructors
	Tile();
	Tile(
		const Side a_top,
		const Side a_right,
		const Side a_bottom,
		const Side a_left,
		const Side a_topRight,
		const Side a_topLeft,
		const Side a_bottomRight,
		const Side a_bottomLeft,
		const Center a_center
	);
	Tile(
		const Side a_top,
		const Side a_right,
		const Side a_bottom,
		const Side a_left,
		const Side a_topRight,
		const Side a_topLeft,
		const Side a_bottomRight,
		const Side a_bottomLeft,
		const Center a_center,
		const BonusPoint a_bonusPoint
	);
	Tile(const Tile &a_tile);
	Tile(Tile && a_tile);

	//destrctor
	~Tile();

	//overloading operators
	auto operator = (const Tile & a_tile)->Tile &;
	auto operator = (Tile && a_tile)->Tile &;


	//getters
	auto getTop() const noexcept->Side;
	auto getTopLeft() const noexcept->Side;
	auto getTopRight() const noexcept->Side;
	auto getRight() const noexcept->Side;
	auto getLeft() const noexcept->Side;
	auto getBottom() const noexcept->Side;
	auto getBottomLeft() const noexcept->Side;
	auto getBottomRight() const noexcept->Side;
	auto getCenter() const noexcept->Center;
	auto getBonusPoint() const noexcept->BonusPoint;
	auto getPiece() noexcept->Piece;
	auto getSideFromZone(const Piece::PieceZone a_zone) const noexcept->Side;
	auto getCenterFromZone(const Piece::PieceZone a_zone) const noexcept->Center;


	//Setters
	auto setTop(const Side a_top) noexcept->void;
	auto setTopLeft(const Side a_topLeft) noexcept->void;
	auto setTopRight(const Side a_topRight) noexcept->void;
	auto setRight(const Side a_right) noexcept->void;
	auto setLeft(const Side a_left) noexcept->void;
	auto setbottom(const Side a_bottom) noexcept->void;
	auto setbottomLeft(const Side a_bottomLeft) noexcept->void;
	auto setbottomRight(const Side a_bottomRight) noexcept->void;
	auto setCenter(const Center a_center) noexcept->void;
	auto setBonusPoint(const BonusPoint a_bonusPoint) noexcept->void;
	auto setPiece(const Piece a_piece) noexcept->void;

	//method for rotating a tile
	auto rotateTileToRight() noexcept->void;

	//Methods for dealing with a piece
	auto hasPiece() noexcept->bool;
	auto placePiece(const Piece& a_piece) noexcept->void;

private:
	Side m_topLeft : 4;
	Side m_top : 4;
	Side m_topRight : 4;
	Side m_right : 4;
	Side m_bottomRight : 4;
	Side m_bottom : 4;
	Side m_bottomLeft : 4;
	Side m_left : 4;
	Center m_center : 4;
	BonusPoint m_bonus : 2;

private:

	//Made with the logic of "A tile can have a piece but also can be without one"
	std::optional<Piece> m_pieceOfTile;
};

