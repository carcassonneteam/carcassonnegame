#pragma once

#include "Tile.h"
#include "Board.h"
#include "../PieceDLL/Piece.h"
#include "PlaceTile.h"

#include <string>
#include <array>
#include <vector>

class Player
{
private:
	static const uint8_t sc_maxNrOfPieces = 7;

public:
	//constructors
	Player(const std::string& a_playerName = "default");
	Player(const Player &a_player);
	Player(Player && a_player);

	//destructor
	~Player() = default;

	//getters
	auto getPlayerName() const noexcept -> const std::string&;
	auto getNrOfPiecesAvailable() const noexcept -> int;
	auto getScore() const noexcept -> int;
	auto getPlacedPieces() const noexcept-> const std::vector<std::pair<uint8_t, uint8_t>>&;

	//setters
	auto setPlayerName(const std::string & a_name) noexcept -> void;
	auto addScore(const int a_score) -> void;
	auto removePiecesFromBoard(std::vector<Board::Position>& a_positions) ->void;

	//overloaded operators
	auto operator=(const Player& a_player)->Player&;
	auto operator=(Player && a_player)->Player &;
	auto operator==(const Player& a_otherPlayer) ->bool;

	//methods for dealing with tiles and pieces
	auto addPieceOnTile(Piece & a_piece, std::pair<Tile, std::string> & a_tile, Piece::PieceZone a_pieceZone, Board::Position & a_tilePosition) -> void;
	auto addTileOnBoard(std::pair<Tile, std::string> && a_tile, Board::Position & a_position, Board & a_board) -> void;

private:
	std::string m_playerName;
	int m_score;
	int m_nrOfPiecesAvailable;
	std::vector<Board::Position> m_placedPiecesOnBoard;
};

