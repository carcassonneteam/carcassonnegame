#pragma once

#include "Tile.h"
#include "../Logging/Logging.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <vector>
#include <random>

/*
UnusedTiles class
This class generates all the tiles of the game,
keeps them in a constant acces time container.
Allso returns a random tile and the start tile of game
*/
class UnusedTiles
{
public:
	using Id = std::size_t;
	using TileAndName = std::pair<Tile, std::string>;

public:
	//constructor
	UnusedTiles();

	//getters
	auto getTile()->TileAndName;
	auto getStartTile()->TileAndName;
	auto getNumberOfRemaningTiles()->size_t;

	//destructor
	~UnusedTiles() = default;

private:
	auto generateTiles() noexcept -> void;

	auto generateSpecificTile(
		const std::string& a_tileName,
		size_t a_numberOfTimes,
		const Tile::Side a_top,
		const Tile::Side a_right,
		const Tile::Side a_bottom,
		const Tile::Side a_left,
		const Tile::Side a_topRight,
		const Tile::Side a_topLeft,
		const Tile::Side a_bottomRight,
		const Tile::Side a_bottomLeft,
		const Tile::Center a_center,
		const Tile::BonusPoint a_bonusPoint = Tile::BonusPoint::None
	) noexcept -> void;

	auto generateStartTile() noexcept ->void;
	auto generateTileType_a() noexcept -> void;
	auto generateTileType_b() noexcept -> void;
	auto generateTileType_c() noexcept -> void;
	auto generateTileType_c1() noexcept -> void;
	auto generateTileType_d() noexcept -> void;
	auto generateTileType_d1() noexcept -> void;
	auto generateTileType_e() noexcept -> void;
	auto generateTileType_f() noexcept -> void;
	auto generateTileType_f1() noexcept -> void;
	auto generateTileType_g() noexcept -> void;
	auto generateTileType_h() noexcept -> void;
	auto generateTileType_h1() noexcept -> void;
	auto generateTileType_i() noexcept -> void;
	auto generateTileType_i1() noexcept -> void;
	auto generateTileType_j() noexcept -> void;
	auto generateTileType_k() noexcept -> void;
	auto generateTileType_l() noexcept -> void;
	auto generateTileType_m() noexcept -> void;
	auto generateTileType_n() noexcept -> void;
	auto generateTileType_o() noexcept -> void;
	auto generateTileType_p() noexcept -> void;
	auto generateTileType_q() noexcept -> void;
	auto generateTileType_r() noexcept -> void;
	auto generateTileType_s() noexcept -> void;
	
private:
	const size_t mc_totalNumberOfPieces = 72;

private:
	std::unordered_map<Id, TileAndName> m_pool;
	TileAndName m_startTile;
	Id m_keyPool;
};

