#pragma once
#include "Tile.h"

#include <array>
#include <optional>

class Board
{
public:
	using Position = std::pair<uint8_t, uint8_t>;

public:
	//constructor
	Board() = default;

	//destructor
	~Board() = default;
	
	//overloaded operators
	auto operator[] (const Position& a_position) const -> const std::optional<std::pair<Tile, std::string>>&;
	auto operator[] (const Position& a_position)->std::optional<std::pair<Tile, std::string>>&;

	//setter
	auto setStartTile(std::pair<Tile, std::string>&& a_tile) -> void;

	//getters
	auto getWidth() const -> size_t;
	auto getHeight() const -> size_t;

private:
	static const size_t sc_width = 144;
	static const size_t sc_height = 144;
	static const size_t sc_size = sc_width * sc_height;

private:
	std::array<std::optional<std::pair<Tile, std::string>>, sc_size> m_board;
};

