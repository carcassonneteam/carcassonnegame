#include "Board.h"

auto Board::operator[](const Position& a_position) const -> const std::optional<std::pair<Tile, std::string>>&
{
	const auto&[l_row, l_column] = a_position;

	if (l_row >= sc_height || l_column >= sc_width)
		throw std::exception("Board index out of bound.");

	return m_board[l_row * sc_width + l_column];
}

auto Board::operator[](const Position& a_position) -> std::optional<std::pair<Tile, std::string>>&
{
	const auto&[l_row, l_column] = a_position;

	if (l_row >= sc_height || l_column >= sc_width)
		throw std::exception("Board index out of bound.");

	return m_board[l_row * sc_width + l_column];
}

auto Board::setStartTile(std::pair<Tile, std::string> && a_tile) -> void
{
	size_t l_centerPosition = (sc_width / 2)*sc_width + (sc_height / 2);
	m_board.at(l_centerPosition) = a_tile;

}

auto Board::getWidth() const -> size_t
{
	return sc_width;
}

auto Board::getHeight() const -> size_t
{
	return sc_height;
}
