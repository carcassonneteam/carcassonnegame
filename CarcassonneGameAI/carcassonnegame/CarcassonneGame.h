#pragma once
#include "Tile.h"
#include "UnusedTiles.h"
#include "Menu.h"
#include "GameScreen.h"

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


class CarcassonneGame
{
public:
	//constr
	CarcassonneGame();

	//destructor
	~CarcassonneGame();

	auto run() -> void;

private:
	std::vector<Screen*> Screens;
	int currentScreen;
	bool musicPlaying;
};

