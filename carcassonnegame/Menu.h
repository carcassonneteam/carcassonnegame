#pragma once
#include <SFML/Graphics.hpp>
#include <array>
#include <string>
#include "Screen.h"

class Menu: public Screen
{
public:
	Menu(const float a_width, const float a_height);
	~Menu() = default;

	virtual auto Run(sf::RenderWindow &a_window, bool & a_musicPlaying) -> int override;

private:
	auto draw(sf::RenderWindow& a_window) -> void;
	auto moveUp() -> void;
	auto moveDown() -> void;
	auto getPressedItem() -> int;
	auto setAtributesForShapes(sf::RectangleShape & a_shape, const sf::Vector2f & a_size, const sf::Color & a_fillColor, const sf::Color & a_outlineColor, float a_outlineThickness, const sf::Vector2f & a_position) -> void;
	auto setAtributesForShapes(sf::RectangleShape & a_hape, const sf::Color & a_fillColor, const sf::Color & a_outlineColor, float a_outlineThickness) -> void;
	auto setAtributesForText(sf::Text & a_elementMenu, const sf::Font & a_font, const sf::Color & a_color, const sf::String & a_string, const sf::Vector2f & a_position) -> void;

private:
	static const int sc_maxNumberOfItems = 3;

private:
	int m_selectedItemIndex;
	sf::Font m_font;
	sf::Text m_menu[sc_maxNumberOfItems];
	sf::RectangleShape m_shapes[sc_maxNumberOfItems];

};

