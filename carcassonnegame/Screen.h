#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

class Screen
{
public:
	virtual auto Run(sf::RenderWindow& window, bool& musicPlaying) -> int = 0;
};

