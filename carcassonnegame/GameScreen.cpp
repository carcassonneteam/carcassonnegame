#include "GameScreen.h"
#include "Game.h"

auto GameScreen::loadTextures() -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	std::ifstream l_fin;
	try
	{
		l_fin.open("tileNames.txt");
		if (!l_fin.is_open())
			throw "Error loading tileNames file";
	}
	catch (const char* excetpion)
	{
		log.log(excetpion, Logger::Level::Error);
		exit(0);
	}
	size_t l_contor = 0;
	while (!l_fin.eof())
	{
		std::string l_nameOfTile;
		l_fin >> l_nameOfTile;
		m_namesOfTiles.push_back(l_nameOfTile);
	}
	l_fin.close();
	try
	{
		l_fin.open("tileTextures.txt");
		if (!l_fin.is_open())
			throw "Error loading tileNames file";
	}
	catch (const char* excetpion)
	{
		log.log(excetpion, Logger::Level::Error);
		exit(0);
	}
	while (!l_fin.eof())
	{
		std::string l_nameOfTileTexture;
		l_fin >> l_nameOfTileTexture;
		if (!m_textures[m_namesOfTiles[l_contor++]].loadFromFile(l_nameOfTileTexture))
		{
			std::string l_errorMessage = "Failed to load tile ";
			std::string l_tileType = m_namesOfTiles[l_contor - 1];
			l_errorMessage.append(l_tileType);
			log.log(l_errorMessage, Logger::Level::Error);
			exit(0);
		}
		else
		{
			std::string l_errorMessage = "Succesful loaded tile ";
			std::string l_tileType = m_namesOfTiles[l_contor - 1];
			l_errorMessage.append(l_tileType);
			log.log(l_errorMessage, Logger::Level::Info);
		}
	}
	l_fin.close();

	for (size_t i = 0; i < m_textures.size(); i++)
	{
		sf::RectangleShape tilee(sf::Vector2f(mc_kTextureWidth, mc_kTextureHight));
		tilee.setTexture(&m_textures.at(m_namesOfTiles[i]));
		m_tilesOfGame[m_namesOfTiles[i]] = tilee;
	}
	of.close();
}

auto GameScreen::loadMusicFile(bool& a_musicPlaying) -> void
{
	if (a_musicPlaying)
	{
		try
		{
			if (!m_music.openFromFile("BackgroundMusic.ogg"))
				throw "Error loading file";
			m_music.play();
			m_music.setVolume(35);
		}
		catch (char* exception)
		{
			std::ofstream of("app.log", std::ios::app);
			Logger log(of);
			log.log(exception, Logger::Level::Error);
			of.close();
		}
	}
}

auto GameScreen::configureRenderWindow(sf::RenderWindow & a_window) -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	a_window.setSize(sf::Vector2u(sf::VideoMode::getFullscreenModes()[4].width, sf::VideoMode::getFullscreenModes()[4].height));
	a_window.setPosition(sf::Vector2i(0, 0));
	m_view = sf::View(sf::Vector2f(mc_kTextureWidth * mc_kViewConstant + mc_kTextureCenter, mc_kTextureHight * mc_kViewConstant + mc_kTextureCenter), static_cast <sf::Vector2f> (a_window.getSize()));
	a_window.setView(m_view);
	log.log("Render window configured...", Logger::Level::Info);
	of.close();
}

auto GameScreen::loadSwitchPlayerButton(sf::RenderWindow& a_window) -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	m_switchPlayer = sf::RectangleShape(sf::Vector2f(150, 40));
	m_switchPlayer.setFillColor(sf::Color(255, 171, 0, 255));
	m_switchPlayer.setOutlineColor(sf::Color::Black);
	m_switchPlayer.setOutlineThickness(2);
	m_switchPlayer.setOrigin(sf::Vector2f(75, 20));
	m_switchPlayer.setPosition(m_view.getCenter().x + a_window.getSize().x / 2 - 100, m_view.getCenter().y + a_window.getSize().y / 2 - 45);
	loadFont();

	setTextOnButton(m_switchPlayerText, "SWITCH PLAYER", std::make_pair(111, 20), m_switchPlayer);
	log.log("Created the switch player button...", Logger::Level::Info);
	of.close();
}

auto GameScreen::loadPlaceMeepleButton(sf::RenderWindow& a_window) -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	m_placeMeeple = sf::RectangleShape(sf::Vector2f(150, 40));
	m_placeMeeple.setFillColor(sf::Color(255, 171, 0, 255));
	m_placeMeeple.setOutlineColor(sf::Color::Black);
	m_placeMeeple.setOutlineThickness(2);
	m_placeMeeple.setOrigin(sf::Vector2f(75, 20));
	m_placeMeeple.setPosition(m_view.getCenter().x + a_window.getSize().x / 2 - 100, m_view.getCenter().y + a_window.getSize().y / 2 - 100);

	setTextOnButton(m_placeMeepleText, "PLACE MEEPLE", std::make_pair(105, 20), m_placeMeeple);
	log.log("Created the place meeple button...", Logger::Level::Info);
	of.close();
}

auto GameScreen::setTextOnButton(sf::Text& a_graphicText, const std::string& a_text, Position pos, const sf::RectangleShape& a_textBox) -> void
{
	auto&[x, y] = pos;
	a_graphicText.setFont(m_font);
	a_graphicText.setFillColor(sf::Color::Black);
	a_graphicText.setString(a_text);
	a_graphicText.setScale(sf::Vector2f(0.5, 0.5));
	a_graphicText.setOrigin(static_cast<float>(x), static_cast<float>(y));
	a_graphicText.setPosition(a_textBox.getPosition());
}

auto GameScreen::loadFont() -> void
{
	try
	{
		if (!m_font.loadFromFile("ALGER.ttf"))
			throw "Can't load the font text for the \"switch player button\". ";
	}
	catch (const char* exception)
	{
		std::ofstream of("app.log", std::ios::app);
		Logger log(of);
		log.log(exception, Logger::Level::Error);
		of.close();
	}
}

auto GameScreen::initializeComponents(sf::RenderWindow & a_window, bool & a_musicPlaying) -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	log.log("Starting initializing components...", Logger::Level::Info);
	loadMusicFile(a_musicPlaying);
	configureRenderWindow(a_window);
	loadTextures();
	loadSwitchPlayerButton(a_window);
	loadPlaceMeepleButton(a_window);


	m_running = true;
	m_placeMeepleNow = false;
	m_rotateTile = true;
	m_changeTilePostion = true;
	m_player = 1;
	log.log("Succes on initializing components...", Logger::Level::Info);
	of.close();
}

auto GameScreen::moveView(sf::RenderWindow& a_window, int a_xPosition, int a_yPosition) noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	m_view.move(static_cast<float>(a_xPosition), static_cast<float>(a_yPosition));
	a_window.setView(m_view);
	m_switchPlayer.setPosition(m_view.getCenter().x + a_window.getSize().x / 2 - 100, m_view.getCenter().y + a_window.getSize().y / 2 - 45);
	m_switchPlayerText.setPosition(m_switchPlayer.getPosition());
	m_placeMeeple.setPosition(m_view.getCenter().x + a_window.getSize().x / 2 - 100, m_view.getCenter().y + a_window.getSize().y / 2 - 100);
	m_placeMeepleText.setPosition(m_placeMeeple.getPosition());
	std::get<1>(m_placedTiles[m_placedTiles.size() - 1]).setPosition(m_view.getCenter().x - a_window.getSize().x / 2 + mc_kTextureCenter, m_view.getCenter().y - a_window.getSize().y / 2 + mc_kTextureCenter);
	log.log("View moved...", Logger::Level::Info);
	of.close();
}

auto GameScreen::changeVisualsToButtons(sf::Text& a_text, sf::RectangleShape& a_button, const sf::Color & a_firstColor, const sf::Color & a_secondColor, size_t a_outLineThickness) -> void
{
	a_text.setFillColor(a_firstColor);
	a_button.setFillColor(a_secondColor);
	a_button.setOutlineColor(a_firstColor);
	a_button.setOutlineThickness(static_cast<float>(a_outLineThickness));
}

auto GameScreen::placeTile(UnusedTiles::TileAndName& a_startTile, std::pair<float, float> a_position) noexcept -> void
{
	if (a_startTile.first.getTop() != Tile::Side::None)//the last tile hase none on top
	{
		std::ofstream of("app.log", std::ios::app);
		Logger log(of);
		auto&[x, y] = a_position;
		//caut acel "tile" (ca parte grafica) in acel map prin string-ul de nume
		m_currentTileGraphic = m_tilesOfGame[a_startTile.second];
		m_currentTileGraphic.setOrigin(mc_kTextureCenter, mc_kTextureCenter);
		m_currentTileGraphic.setPosition(x, y);
		//o pun in map-ul placed piece(asta e facut sa tinem minte grafica)
		m_placedTiles.push_back(std::make_tuple(std::make_pair(static_cast<int>(m_currentTileGraphic.getPosition().x), static_cast<int>(m_currentTileGraphic.getPosition().y)), m_currentTileGraphic, a_startTile));
		log.log("Tile was moved on screen...", Logger::Level::Info);
		of.close();
	}
}

auto GameScreen::changePlayer() noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	m_player++;
	m_placeMeepleNow = false;
	m_changeTilePostion = true;
	m_rotateTile = true;
	log.log("Switched players...", Logger::Level::Info);
	of.close();
}

auto GameScreen::changeTilePosition(Position & a_graphicPosition, Position & a_logicPosition, Position a_positionPool) noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	a_graphicPosition = a_positionPool;
	a_graphicPosition.first = (a_graphicPosition.first / static_cast<int>(mc_kTextureWidth)) * static_cast<int>(mc_kTextureWidth) + static_cast<int>(mc_kTextureCenter);
	a_graphicPosition.second = (a_graphicPosition.second / static_cast<int>(mc_kTextureHight)) * static_cast<int>(mc_kTextureHight) + static_cast<int>(mc_kTextureCenter);
	a_logicPosition.first = a_graphicPosition.first / static_cast<int>(mc_kTextureWidth);
	a_logicPosition.second = a_graphicPosition.second / static_cast<int>(mc_kTextureHight);
	log.log("Tile position changed...", Logger::Level::Info);
	of.close();
}

auto GameScreen::placePieceOnTile(Position & a_graphicPosition, Position& a_logicTilePosition) noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	sf::CircleShape l_circle;
	if (m_player % 2 == 0)
		l_circle.setFillColor(sf::Color::Yellow);
	else
		l_circle.setFillColor(sf::Color::Blue);

	a_graphicPosition.first = (a_graphicPosition.first / 30) * 30 + 10;
	a_graphicPosition.second = (a_graphicPosition.second / 30) * 30 + 10;

	l_circle.setRadius(10);
	l_circle.setOrigin(5, 5);
	l_circle.setPosition(static_cast<float>(a_graphicPosition.first), static_cast<float>(a_graphicPosition.second));
	m_meeples.push_back(std::make_pair(a_logicTilePosition, l_circle));
	m_placeMeepleNow = false;
	log.log("Piece was placed on a tile...", Logger::Level::Info);
	of.close();
}

auto GameScreen::getPlayerNameFromUser(std::string& a_playerName) noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);

	std::cout << "Enter the player name: ";
	std::cin >> a_playerName;

	log.log("A player name was entered...", Logger::Level::Info);
	of.close();
}

auto GameScreen::erasePieceFromScreen() noexcept -> void
{
	if (m_meeples.size() > 1)
	{
		std::vector<std::vector<std::pair<Position, sf::CircleShape>>::iterator> l_toErase;
		for (auto& l_pieceToErase : m_piecesToRemoveFromTiles)
		{
			for (auto l_piece = m_meeples.begin(); l_piece != m_meeples.end(); ++l_piece)
			{
				auto&[tileCoordX, tileCoordY] = l_piece->first;
				if (static_cast<uint8_t>(tileCoordX) == l_pieceToErase.first && static_cast<uint8_t>(tileCoordY) == l_pieceToErase.second)
					l_toErase.push_back(l_piece);
			}
		}
		for (auto l_erase : l_toErase)
			m_meeples.erase(l_erase);
	}
	else
	{
		auto l_piece = m_meeples.begin();
		auto&[tileCoordX, tileCoordY] = l_piece->first;
		for (auto& l_pieceToErase : m_piecesToRemoveFromTiles)
		{
			if (static_cast<uint8_t>(tileCoordX) == l_pieceToErase.first && static_cast<uint8_t>(tileCoordY) == l_pieceToErase.second)
				m_meeples.erase(l_piece);
		}
	}

	m_piecesToRemoveFromTiles.clear();
}

auto GameScreen::getLogicTilePosition(Position a_tileGraphicPosition) noexcept -> Position
{
	Position l_tilelogicPosition;
	a_tileGraphicPosition.first = (a_tileGraphicPosition.first / static_cast<int>(mc_kTextureWidth)) * static_cast<int>(mc_kTextureWidth) + static_cast<int>(mc_kTextureCenter);
	a_tileGraphicPosition.second = (a_tileGraphicPosition.second / static_cast<int>(mc_kTextureHight)) * static_cast<int>(mc_kTextureHight) + static_cast<int>(mc_kTextureCenter);
	l_tilelogicPosition.first = a_tileGraphicPosition.first / static_cast<int>(mc_kTextureWidth);
	l_tilelogicPosition.second = a_tileGraphicPosition.second / static_cast<int>(mc_kTextureHight);
	return l_tilelogicPosition;
}

auto GameScreen::drawComponents(sf::RenderWindow & a_window) noexcept -> void
{
	a_window.clear(sf::Color(255, 196, 118, 255));

	for (auto & l_sprites : m_placedTiles)
		a_window.draw(std::get<1>(l_sprites));
	a_window.draw(m_switchPlayer);
	a_window.draw(m_switchPlayerText);
	a_window.draw(m_placeMeeple);
	a_window.draw(m_placeMeepleText);
	for (auto & l_meeple : m_meeples)
		a_window.draw(l_meeple.second);
	a_window.display();
}

auto GameScreen::Run(sf::RenderWindow& a_window, bool & a_musicPlaying) -> int
{
	initializeComponents(a_window, a_musicPlaying);

	std::string l_firstPlayerName, l_secondPlayerName;
	getPlayerNameFromUser(l_firstPlayerName);
	getPlayerNameFromUser(l_secondPlayerName);

	Game l_game(l_firstPlayerName, l_secondPlayerName);

	auto l_startTile = l_game.getFirstTile();//getStartTile
	placeTile(l_startTile, std::make_pair(mc_kTextureWidth * mc_kViewConstant + mc_kTextureCenter, mc_kTextureHight * mc_kViewConstant + mc_kTextureCenter));//placeIt

	l_game.setCurrentTile(l_game.getNextTileToPlace());//getNextTile
	auto l_tile = l_game.getCurrentTile();
	placeTile(l_tile, std::make_pair(m_view.getCenter().x - a_window.getSize().x / 2 + mc_kTextureCenter, m_view.getCenter().y - a_window.getSize().y / 2 + mc_kTextureCenter));//placeIt

	l_game.setCurrentPlayer(true);

	//mainLoop
	while (m_running && l_game.getPoolOfTiles().getNumberOfRemaningTiles() > 0)
	{
		sf::Event l_event;
		while (a_window.pollEvent(l_event))
		{
			switch (l_event.type)
			{
			case sf::Event::Closed:
				a_window.close();
				return -1;
				/*When the mouse is over the button, the button show itself more, when
				the mouse gets back to not beeing over the button, the button goes to normal*/
			case sf::Event::MouseMoved:
				if (m_switchPlayer.getGlobalBounds().contains(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseMove.x, l_event.mouseMove.y))))
					changeVisualsToButtons(m_switchPlayerText, m_switchPlayer, sf::Color(100, 0, 0, 255), sf::Color(255, 220, 0, 255), 5);
				else
					changeVisualsToButtons(m_switchPlayerText, m_switchPlayer, sf::Color(0, 0, 0, 255), sf::Color(255, 171, 0, 255), 2);
				if (m_placeMeeple.getGlobalBounds().contains(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseMove.x, l_event.mouseMove.y))))
					changeVisualsToButtons(m_placeMeepleText, m_placeMeeple, sf::Color(100, 0, 0, 255), sf::Color(255, 220, 0, 255), 5);
				else
					changeVisualsToButtons(m_placeMeepleText, m_placeMeeple, sf::Color(0, 0, 0, 255), sf::Color(255, 171, 0, 255), 2);
				break;

			case sf::Event::KeyPressed:
				if (l_event.key.code == sf::Keyboard::Escape)
				{
					a_window.close();
					return -1;
				}
				else if (l_event.key.code == sf::Keyboard::Left)
					moveView(a_window, -10, 0);
				else if (l_event.key.code == sf::Keyboard::Right)
					moveView(a_window, 10, 0);
				else if (l_event.key.code == sf::Keyboard::Up)
					moveView(a_window, 0, -10);
				else if (l_event.key.code == sf::Keyboard::Down)
					moveView(a_window, 0, 10);
				break;
			case sf::Event::MouseButtonReleased:
				if (l_event.mouseButton.button == sf::Mouse::Button::Left)
				{
					//rotateTile & placeMeeple
					//decided by those bools
					if (std::get<1>(m_placedTiles[m_placedTiles.size() - 1]).getGlobalBounds().contains(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y))))
					{
						//If a tile is rotateable;
						if (!m_placeMeepleNow)
						{
							if (m_rotateTile)
							{
								std::get<1>(m_placedTiles[m_placedTiles.size() - 1]).rotate(90.0f);//partea grafica
								std::get<2>(m_placedTiles[m_placedTiles.size() - 1]).first.rotateTileToRight();
								l_game.setCurrentTile(std::get<2>(m_placedTiles[m_placedTiles.size() - 1]));
							}
						}
						//if a tile is NOT rotateable
						else
						{
							//Wiht op % on position i can see exactly where the cursor is in tiles coordinates
							//So in this way i can see clearly where on tile whants the user to place a meeple
							Position l_graphicPiecePosition;
							Position l_logicPiecePosition;
							Position l_tilelogicPosition;
							l_tilelogicPosition = getLogicTilePosition(std::make_pair(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y)).x, a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y)).y));

							l_graphicPiecePosition = std::make_pair(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y)).x, a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y)).y);
							l_logicPiecePosition.first = l_graphicPiecePosition.first % static_cast<int>(mc_kTextureWidth);
							l_logicPiecePosition.second = l_graphicPiecePosition.second % static_cast<int>(mc_kTextureHight);

							auto l_verif = l_game.placePieceOnTile(l_logicPiecePosition);

							if (!l_verif)
								placePieceOnTile(l_graphicPiecePosition, l_tilelogicPosition);
						}
					}
					//Verify if switch player button was pressed
					else if (m_switchPlayer.getGlobalBounds().contains(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y))))
					{
						//before switching the players, we verify if there are finished road/city/cloister and add points to each player
						l_game.addPointsToPlayers(m_piecesToRemoveFromTiles);
						erasePieceFromScreen();

						//For now we simulate play of 2 players, for more, the game mecanic will work the same.
						l_game.switchPlayers(m_player % 2 != 1);
						//We give the player another tile
						changePlayer();
						l_game.setCurrentTile(l_game.getNextTileToPlace());
						l_tile = l_game.getCurrentTile();
						placeTile(l_tile, std::make_pair(m_view.getCenter().x - a_window.getSize().x / 2 + mc_kTextureCenter, m_view.getCenter().y - a_window.getSize().y / 2 + mc_kTextureCenter));
					}
					//verify if placeMeepleWasPresed
					else if (m_placeMeeple.getGlobalBounds().contains(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y))))
					{
						m_placeMeepleNow = true;
					}
					//try to place tile on a position
					else
					{
						if (m_changeTilePostion)
						{
							//Actualize the curent tile position both logicaly(with l_game obj) and graphical
							std::pair<int, int> l_newPosition;
							std::pair<int, int> l_logicPosition;
							changeTilePosition(l_newPosition, l_logicPosition, std::make_pair(a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y)).x, a_window.mapPixelToCoords(sf::Vector2i(l_event.mouseButton.x, l_event.mouseButton.y)).y));
							bool l_placed = l_game.placeTileOnBoard(std::move(l_game.getCurrentTile()), l_logicPosition);
							//Se if the position selected by the player is valid
							if (l_placed)
							{
								std::get<0>(m_placedTiles[m_placedTiles.size() - 1]) = l_newPosition;
								std::get<1>(m_placedTiles[m_placedTiles.size() - 1]).setPosition(sf::Vector2f(static_cast<float>(l_newPosition.first), static_cast<float>(l_newPosition.second)));
								m_changeTilePostion = false;
								m_rotateTile = false;
							}
							else
								std::cout << "Incorrect position\n";
						}
					}
				}
				break;
			}
		}
		drawComponents(a_window);
	}

	//here we calculate the final points of game
	if (l_game.getPoolOfTiles().getNumberOfRemaningTiles() == 0)
	{
		l_game.addPointsToPlayersAtTheEnd(m_piecesToRemoveFromTiles);
		m_meeples.clear();

		system("pause");
	}

	return -1;
}