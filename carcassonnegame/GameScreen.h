#pragma once
#include "Screen.h"
#include "UnusedTiles.h"
#include "Player.h"

#include <string>
#include <unordered_map>

class GameScreen : public Screen
{
private:
	using Position = std::pair<int, int>;
	const float mc_kTextureWidth = 90.0f;
	const float mc_kTextureHight = 90.0f;
	const float mc_kTextureCenter = 45.0f;

	const float mc_kViewConstant = 72.0f;

public:
	GameScreen() = default;
	~GameScreen() = default;

private:
	//loading methods
	auto loadTextures() -> void;
	auto loadMusicFile(bool& a_musicPlaying) -> void;
	auto configureRenderWindow(sf::RenderWindow& a_window) -> void;
	auto loadSwitchPlayerButton(sf::RenderWindow& a_window) -> void;
	auto loadPlaceMeepleButton(sf::RenderWindow& a_window) -> void;
	auto setTextOnButton(sf::Text& a_graphicText, const std::string& a_text, Position pos, const sf::RectangleShape& a_textBox) -> void;
	auto loadFont() -> void;
	auto initializeComponents(sf::RenderWindow& a_window, bool& a_musicPlaying) -> void;

	//other kind of methods
	auto moveView(sf::RenderWindow& a_window, int a_xPosition, int a_yPosition) noexcept -> void;
	auto changeVisualsToButtons(sf::Text& a_text, sf::RectangleShape& a_button, const sf::Color& a_firstColor, const sf::Color& a_secondColor, size_t a_outLineThickness) -> void;
	auto placeTile(UnusedTiles::TileAndName& a_startTile, std::pair<float, float> a_position) noexcept -> void;
	auto changePlayer() noexcept -> void;
	auto changeTilePosition(Position& a_graphicPosition, Position& a_logicPosition, Position a_positionPool) noexcept -> void;
	auto placePieceOnTile(Position& a_graphicPosition, Position& a_logicTilePosition) noexcept -> void;
	auto getPlayerNameFromUser(std::string& a_playerName) noexcept -> void;
	auto erasePieceFromScreen() noexcept -> void;
	auto getLogicTilePosition(Position a_tileGraphicPosition) noexcept->Position;

	//draw
	auto drawComponents(sf::RenderWindow& a_window) noexcept -> void;

public:
	virtual auto Run(sf::RenderWindow& a_window, bool& a_musicPlaying)->int override;

private:
	sf::View m_view;
	std::unordered_map <std::string, sf::RectangleShape> m_tilesOfGame;
	std::unordered_map <std::string, sf::Texture> m_textures;
	std::vector <std::string> m_namesOfTiles;
	std::vector <std::tuple<Position, sf::RectangleShape, UnusedTiles::TileAndName>> m_placedTiles;
	std::vector<std::pair<Position, sf::CircleShape>> m_meeples;
	sf::Music m_music;

	std::vector<Board::Position> m_piecesToRemoveFromTiles;

private:
	//Here are some buttons
	sf::RectangleShape m_switchPlayer;
	sf::RectangleShape m_placeMeeple;

	//font & texts
	sf::Font m_font;
	sf::Text m_switchPlayerText;
	sf::Text m_placeMeepleText;

	//some functionality
	//player1 has uneven turn / player2 has even turn
	size_t m_player;
	sf::RectangleShape m_currentTileGraphic;
	bool m_running;
	bool m_placeMeepleNow;
	bool m_rotateTile;
	bool m_changeTilePostion;
};

