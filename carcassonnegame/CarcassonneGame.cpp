#include "CarcassonneGame.h"

CarcassonneGame::CarcassonneGame()
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Application started...", Logger::Level::Info);
	of.close();
	currentScreen = 0;
	musicPlaying = true;
}

CarcassonneGame::~CarcassonneGame()
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	Screens.clear();
	log.log("Application ended...", Logger::Level::Info);
	of.close();
}

auto CarcassonneGame::run() -> void
{
	sf::RenderWindow window(sf::VideoMode(600, 600), "CARCASSONNE", sf::Style::Titlebar | sf::Style::Close);
	Menu menu(static_cast<float>(window.getSize().x), static_cast<float>(window.getSize().y));
	Screens.push_back(&menu);
	GameScreen myGame;
	Screens.push_back(&myGame);
	if (currentScreen == 0)
	{
		std::ofstream of("app.log", std::ios::app);
		Logger log(of);
		log.log("Menu window started...", Logger::Level::Info);
		currentScreen = Screens[currentScreen]->Run(window, musicPlaying);
		log.log("Menu window ended...", Logger::Level::Info);
		of.close();
	}
	if (currentScreen >= 0)
	{
		std::ofstream of("app.log", std::ios::app);
		Logger log(of);
		log.log("Game window started...", Logger::Level::Info);
		currentScreen = Screens[currentScreen]->Run(window, musicPlaying);
		log.log("Game succesfuly ended...", Logger::Level::Info);
		of.close();
	}
}
