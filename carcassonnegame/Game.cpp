#include "Game.h"
#include "PlaceTile.h"

Game::Game(const std::string& a_nameFirstPlayer, const std::string& a_nameSecondPlayer)
{
	m_player1 = std::make_unique<Player>(a_nameFirstPlayer);
	m_player2 = std::make_unique<Player>(a_nameSecondPlayer);
	m_currentPlayer = nullptr;
}

auto Game::getPoolOfTiles() -> UnusedTiles
{
	return m_poolOfTiles;
}

auto Game::getCurrentTile() -> UnusedTiles::TileAndName
{
	return m_currentTile;
}

auto Game::getNextTileToPlace() -> UnusedTiles::TileAndName
{
	return m_poolOfTiles.getTile();
}

auto Game::getCurrentPlayer() -> bool
{
	if (m_currentPlayer->getPlayerName() == m_player1->getPlayerName())
	{
		m_player1 = std::move(m_currentPlayer);
		return true;
	}
	m_player2 = std::move(m_currentPlayer);
	return false;
}

auto Game::getPlayer() const -> Player
{
	return *m_currentPlayer;
}

auto Game::getFirstTile() -> UnusedTiles::TileAndName
{
	auto l_tile = m_poolOfTiles.getStartTile();
	m_board.setStartTile({ l_tile.first, l_tile.second });
	return l_tile;
}

auto Game::getPieceOfCurrentTile() -> Piece
{

	if (m_currentTile.first.hasPiece())
		return m_currentTile.first.getPiece();
	else
	{
		Piece piece(Piece::PieceZone::None);
		return piece;
	}
}

//auto Game::setPlayers(Player & player1, Player & player2) noexcept -> void
//{
//	m_player1 = player1;
//	m_player2 = player2;
//}

auto Game::setCurrentPlayer(const bool a_player) noexcept -> void
{
	if (a_player == true)
	{
		m_currentPlayer = std::move(m_player1);
		std::cout << "Player1 turn!" << std::endl;
	}
	else
	{
		m_currentPlayer = std::move(m_player2);
		std::cout << "Player2 turn!" << std::endl;
	}
}

auto Game::setCurrentTile(const UnusedTiles::TileAndName & a_tile) -> void
{
	m_currentTile = a_tile;
}

auto Game::switchPlayers(bool a_player1) -> void
{
	if (a_player1 == true)
	{
		if (!m_player2)
			m_player2 = std::move(m_currentPlayer);
		m_currentPlayer = std::move(m_player1);

		std::cout << "Player1 turn!" << std::endl;
	}
	else
	{
		if (!m_player1)
			m_player1 = std::move(m_currentPlayer);
		m_currentPlayer = std::move(m_player2);

		std::cout << "Player2 turn!" << std::endl;
	}
}

auto Game::placeTileOnBoard(UnusedTiles::TileAndName && a_tile, const Position & a_pos) -> bool
{
	try
	{
		PlaceTile::placeTile(a_pos.first, a_pos.second, std::move(a_tile), m_board);
		m_currentPostition = a_pos;

		return true;
	}
	catch (const std::exception&)
	{
		return false;
	}
}

auto Game::placePieceOnCurrentTile(Piece & a_piece) noexcept -> bool
{
	if (!m_player1)
	{
		Board::Position l_pos = { static_cast<uint8_t>(m_currentPostition.first), static_cast<uint8_t>(m_currentPostition.second) };
		m_player1 = std::move(m_currentPlayer);
		m_playerTurn = true;
		try
		{
			m_player1->addPieceOnTile(a_piece, m_board[m_currentPostition].value(), a_piece.getPieceZone(), l_pos);
			m_currentTile = m_board[m_currentPostition].value();

			return false;
		}
		catch (const std::exception& exception)
		{
			std::cout << exception.what() << std::endl;
			return true;
		}
	}
	else
	{
		Board::Position l_pos = { static_cast<uint8_t>(m_currentPostition.first), static_cast<uint8_t>(m_currentPostition.second) };
		m_player2 = std::move(m_currentPlayer);
		m_playerTurn = false;

		try
		{
			m_player2->addPieceOnTile(a_piece, m_board[m_currentPostition].value(), a_piece.getPieceZone(), l_pos);
			m_currentTile = m_board[m_currentPostition].value();

			return false;
		}
		catch (const std::exception& exception)
		{
			std::cout << exception.what() << std::endl;
			return true;
		}
	}
}

auto Game::verifyPiecePlacement(const Position a_currentPostion, const Piece::PieceZone a_pieceZone) noexcept -> bool
{
	bool l_isThereAnyPieceOnRoad = false;
	bool l_isThereAnyPieceOnCity = false;
	Position l_oldPosition;
	std::vector<Board::Position> l_oldPositions;

	auto l_pieceSide = m_currentTile.first.getSideFromZone(m_currentTile.first.getPiece().getPieceZone());

	if (l_pieceSide == Tile::Side::Road)
	{
		l_oldPosition = { 0,0 };
		PlaceTile::verifyPiecePlacementOnRoad(m_currentPostition, l_oldPosition, a_pieceZone, m_board, l_isThereAnyPieceOnRoad);
	}

	if (l_pieceSide == Tile::Side::City)
	{
		PlaceTile::verifyPiecePlacementOnCity(m_currentPostition, l_oldPositions, a_pieceZone, m_board, l_isThereAnyPieceOnCity);
	}

	return l_isThereAnyPieceOnRoad || l_isThereAnyPieceOnCity;
}

auto Game::methodRoad(std::vector<Board::Position> &m_piecesToRemoveFromTiles) -> void
{
	Board::Position l_position;
	size_t l_points = 0;
	bool l_isRoadFinished = true;
	std::vector<Board::Position> l_piecePositions;
	bool l_verif;

	if (!PlaceTile::areTwoZonesWithRoad(m_currentTile.first))
	{
		l_isRoadFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_position = { 0,0 };
		if (m_currentTile.first.getTop() == Tile::Side::Road)
		{
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Top, l_points, l_isRoadFinished, l_piecePositions);
			if (l_isRoadFinished == true)
			{
				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

		l_isRoadFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_position = { 0,0 };
		if (m_currentTile.first.getBottom() == Tile::Side::Road)
		{
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Bottom, l_points, l_isRoadFinished, l_piecePositions);
			if (l_isRoadFinished == true)
			{
				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

		l_isRoadFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_position = { 0,0 };
		if (m_currentTile.first.getLeft() == Tile::Side::Road)
		{
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Left, l_points, l_isRoadFinished, l_piecePositions);
			if (l_isRoadFinished == true)
			{
				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

		l_isRoadFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_position = { 0,0 };
		if (m_currentTile.first.getRight() == Tile::Side::Road)
		{
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Right, l_points, l_isRoadFinished, l_piecePositions);
			if (l_isRoadFinished == true)
			{
				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points, piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

	}
	else
	{
		l_isRoadFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_position = { 0,0 };
		if (m_currentTile.first.getTop() == Tile::Side::Road)
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Top, l_points, l_isRoadFinished, l_piecePositions);
		else if (m_currentTile.first.getBottom() == Tile::Side::Road)
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Bottom, l_points, l_isRoadFinished, l_piecePositions);
		else if (m_currentTile.first.getLeft() == Tile::Side::Road)
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Left, l_points, l_isRoadFinished, l_piecePositions);
		else if (m_currentTile.first.getRight() == Tile::Side::Road)
			countPointsOfRoad(m_currentPostition, l_position, Piece::PieceZone::Right, l_points, l_isRoadFinished, l_piecePositions);

		if (l_isRoadFinished == true)
		{
			for (auto& piecePosition : l_piecePositions)
			{
				l_verif = m_player1->addScore(l_points, piecePosition);
				if (l_verif)
					m_piecesToRemoveFromTiles.push_back(piecePosition);

				l_verif = m_player2->addScore(l_points, piecePosition);
				if (l_verif)
					m_piecesToRemoveFromTiles.push_back(piecePosition);
			}
		}
	}
}

auto Game::methodCity(std::vector<Board::Position> &m_piecesToRemoveFromTiles) -> void
{
	std::vector<Board::Position> l_oldPositions;
	size_t l_points = 0;
	bool l_isCityFinished = true;
	std::vector<Board::Position> l_piecePositions;
	bool l_verif;

	if (m_currentTile.first.getCenter() != Tile::Center::City)
	{
		l_isCityFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_oldPositions.clear();
		if (m_currentTile.first.getTop() == Tile::Side::City)
		{
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Top, l_points, l_isCityFinished, l_piecePositions);
			if (l_isCityFinished == true)
			{
				for (auto& tilePosition : l_oldPositions)
				{
					if (m_board[tilePosition].value().first.getBonusPoint() == Tile::BonusPoint::True)
						l_points++;
				}

				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

		l_isCityFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_oldPositions.clear();
		if (m_currentTile.first.getBottom() == Tile::Side::City)
		{
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Bottom, l_points, l_isCityFinished, l_piecePositions);
			if (l_isCityFinished == true)
			{
				for (auto& tilePosition : l_oldPositions)
				{
					if (m_board[tilePosition].value().first.getBonusPoint() == Tile::BonusPoint::True)
						l_points++;
				}

				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

		l_isCityFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_oldPositions.clear();
		if (m_currentTile.first.getLeft() == Tile::Side::City)
		{
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Left, l_points, l_isCityFinished, l_piecePositions);
			if (l_isCityFinished == true)
			{
				for (auto& tilePosition : l_oldPositions)
				{
					if (m_board[tilePosition].value().first.getBonusPoint() == Tile::BonusPoint::True)
						l_points++;
				}

				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

		l_isCityFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_oldPositions.clear();
		if (m_currentTile.first.getRight() == Tile::Side::City)
		{
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Right, l_points, l_isCityFinished, l_piecePositions);
			if (l_isCityFinished == true)
			{
				for (auto& tilePosition : l_oldPositions)
				{
					if (m_board[tilePosition].value().first.getBonusPoint() == Tile::BonusPoint::True)
						l_points++;
				}

				for (auto& piecePosition : l_piecePositions)
				{
					l_verif = m_player1->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);

					l_verif = m_player2->addScore(l_points + l_oldPositions.size(), piecePosition);
					if (l_verif)
						m_piecesToRemoveFromTiles.push_back(piecePosition);
				}
			}
		}

	}
	else
	{
		l_isCityFinished = true;
		l_piecePositions.clear();
		l_points = 0;
		l_oldPositions.clear();
		if (m_currentTile.first.getTop() == Tile::Side::City)
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Top, l_points, l_isCityFinished, l_piecePositions);
		else if (m_currentTile.first.getBottom() == Tile::Side::City)
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Bottom, l_points, l_isCityFinished, l_piecePositions);
		else if (m_currentTile.first.getLeft() == Tile::Side::City)
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Left, l_points, l_isCityFinished, l_piecePositions);
		else if (m_currentTile.first.getRight() == Tile::Side::City)
			countPointsOfCity(m_currentPostition, l_oldPositions, Piece::PieceZone::Right, l_points, l_isCityFinished, l_piecePositions);

		if (l_isCityFinished == true)
		{
			for (auto& tilePosition : l_oldPositions)
			{
				if (m_board[tilePosition].value().first.getBonusPoint() == Tile::BonusPoint::True)
					l_points++;
			}

			for (auto& piecePosition : l_piecePositions)
			{
				l_verif = m_player1->addScore(l_points + l_oldPositions.size(), piecePosition);
				if (l_verif)
					m_piecesToRemoveFromTiles.push_back(piecePosition);

				l_verif = m_player2->addScore(l_points + l_oldPositions.size(), piecePosition);
				if (l_verif)
					m_piecesToRemoveFromTiles.push_back(piecePosition);
			}
		}
	}
}

auto Game::methodCloister(std::vector<Board::Position>& m_piecesToRemoveFromTiles) -> void
{
	std::vector<Board::Position> l_piecePositions;
	auto&[l_row, l_column] = m_currentPostition;
	const uint8_t l_noNeighbours = 8;
	bool l_verif;

	const std::array<Board::Position, l_noNeighbours> l_neighbours{ {
			{ static_cast<uint8_t>(l_row - 1) , static_cast<uint8_t>(l_column - 1) },
			{ static_cast<uint8_t>(l_row) , static_cast<uint8_t>(l_column - 1) },
			{ static_cast<uint8_t>(l_row + 1) , static_cast<uint8_t>(l_column - 1) },
			{ static_cast<uint8_t>(l_row + 1) , static_cast<uint8_t>(l_column) },
			{ static_cast<uint8_t>(l_row + 1) , static_cast<uint8_t>(l_column + 1) },
			{ static_cast<uint8_t>(l_row) , static_cast<uint8_t>(l_column + 1) },
			{ static_cast<uint8_t>(l_row - 1) , static_cast<uint8_t>(l_column + 1) },
			{ static_cast<uint8_t>(l_row - 1) , static_cast<uint8_t>(l_column) } } };

	if (m_currentTile.first.getCenter() == Tile::Center::Cloister)
		l_piecePositions.push_back(m_currentPostition);

	for (auto& neighbour : l_neighbours)
	{
		if (m_board[neighbour].has_value() && m_board[neighbour].value().first.getCenter() == Tile::Center::Cloister)
			l_piecePositions.push_back(neighbour);
	}

	for (auto& piecePosition : l_piecePositions)
	{
		size_t l_points = 0;
		bool l_isCloisterFinished = false;
		countPointsCloister(piecePosition, l_points, l_isCloisterFinished);

		if (l_isCloisterFinished == true)
		{
			l_points++;
			l_verif = m_player1->addScore(l_points, piecePosition);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(piecePosition);

			l_verif = m_player2->addScore(l_points, piecePosition);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(piecePosition);
		}
	}
}

auto Game::addPointsToPlayers(std::vector<Board::Position> &m_piecesToRemoveFromTiles) -> void
{
	if (!m_player1)
		m_player1 = std::move(m_currentPlayer);

	if (!m_player2)
		m_player2 = std::move(m_currentPlayer);

	int l_tempScorePlayer1 = m_player1->getScore();
	int l_tempScorePlayer2 = m_player2->getScore();

	methodRoad(m_piecesToRemoveFromTiles);
	std::cout << std::endl;

	methodCity(m_piecesToRemoveFromTiles);
	std::cout << std::endl;

	methodCloister(m_piecesToRemoveFromTiles);
	std::cout << std::endl;

	if (l_tempScorePlayer1 != m_player1->getScore())
		std::cout << "Player1 score: " << m_player1->getScore() << std::endl;

	if (l_tempScorePlayer2 != m_player2->getScore())
		std::cout << "Player2 score: " << m_player2->getScore() << std::endl;

	std::cout << std::endl;
}


auto Game::addPointsToPlayersAtTheEnd(std::vector<Board::Position>& m_piecesToRemoveFromTiles) -> void
{
	if (!m_player1)
		m_player1 = std::move(m_currentPlayer);

	if (!m_player2)
		m_player2 = std::move(m_currentPlayer);

	//player1
	for (auto& l_piece : m_player1->getPlacedPieces())
	{
		auto l_side = m_board[l_piece].value().first.getSideFromZone(m_board[l_piece].value().first.getPiece().getPieceZone());
		auto l_center = m_board[l_piece].value().first.getCenterFromZone(m_board[l_piece].value().first.getPiece().getPieceZone());

		if (l_side == Tile::Side::Road)
		{
			Board::Position l_oldPosition;
			bool l_isRoadFinished = false;
			size_t l_points = 0;
			std::vector<Board::Position> l_piecePosition;
			countPointsOfRoad(l_piece, l_oldPosition, m_board[l_piece].value().first.getPiece().getPieceZone(), l_points, l_isRoadFinished, l_piecePosition);

			bool l_verif = m_player1->addScore(l_points + 1, l_piece);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(l_piece);
		}

		if (l_side == Tile::Side::City)
		{
			std::vector<Board::Position> l_oldPositions;
			bool l_isCityFinished = false;
			size_t l_points = 0;
			std::vector<Board::Position> l_piecePositions;
			countPointsOfCity(l_piece, l_oldPositions, m_board[l_piece].value().first.getPiece().getPieceZone(), l_points, l_isCityFinished, l_piecePositions);

			bool l_verif = m_player1->addScore(l_points + l_oldPositions.size(), l_piece);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(l_piece);
		}

		if (l_center == Tile::Center::Cloister)
		{
			bool l_isCloisterFinished = false;
			size_t l_points = 0;

			countPointsCloister(l_piece, l_points, l_isCloisterFinished);

			bool l_verif = m_player1->addScore(l_points, l_piece);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(l_piece);
		}
	}

	//player2
	for (auto& l_piece : m_player2->getPlacedPieces())
	{
		auto l_side = m_board[l_piece].value().first.getSideFromZone(m_board[l_piece].value().first.getPiece().getPieceZone());
		auto l_center = m_board[l_piece].value().first.getCenterFromZone(m_board[l_piece].value().first.getPiece().getPieceZone());

		if (l_side == Tile::Side::Road)
		{
			Board::Position l_oldPosition;
			bool l_isRoadFinished = false;
			size_t l_points = 0;
			std::vector<Board::Position> l_piecePosition;
			countPointsOfRoad(l_piece, l_oldPosition, m_board[l_piece].value().first.getPiece().getPieceZone(), l_points, l_isRoadFinished, l_piecePosition);

			bool l_verif = m_player2->addScore(l_points + 1, l_piece);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(l_piece);
		}

		if (l_side == Tile::Side::City)
		{
			std::vector<Board::Position> l_oldPositions;
			bool l_isCityFinished = false;
			size_t l_points = 0;
			std::vector<Board::Position> l_piecePositions;
			countPointsOfCity(l_piece, l_oldPositions, m_board[l_piece].value().first.getPiece().getPieceZone(), l_points, l_isCityFinished, l_piecePositions);

			bool l_verif = m_player2->addScore(l_points + l_oldPositions.size(), l_piece);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(l_piece);
		}

		if (l_center == Tile::Center::Cloister)
		{
			bool l_isCloisterFinished = false;
			size_t l_points = 0;

			countPointsCloister(l_piece, l_points, l_isCloisterFinished);

			bool l_verif = m_player2->addScore(l_points, l_piece);
			if (l_verif)
				m_piecesToRemoveFromTiles.push_back(l_piece);
		}
	}

	std::cout << "Player1 score: " << m_player1->getScore() << std::endl;
	std::cout << "Player1 score: " << m_player2->getScore() << std::endl;
	std::cout << std::endl;
}

auto Game::placePieceOnTile(const Position a_currentPostion) noexcept -> bool
{
	Piece l_piece;
	if (a_currentPostion.first > 30 && a_currentPostion.first < 60 && a_currentPostion.second < 30)
	{
		l_piece.setPieceZone(Piece::PieceZone::Top);
		if (m_currentTile.first.getTop() != Tile::Side::Filed)
		{
			m_currentTile.first.placePiece(l_piece);
			auto l_verif = verifyPiecePlacement(a_currentPostion, l_piece.getPieceZone());
			if (!l_verif)
			{
				l_verif = placePieceOnCurrentTile(l_piece);
			}
			return l_verif;
		}
	}
	else if (a_currentPostion.first < 30 && a_currentPostion.second > 30 && a_currentPostion.second < 60)
	{
		l_piece.setPieceZone(Piece::PieceZone::Left);
		if (m_currentTile.first.getLeft() != Tile::Side::Filed)
		{
			m_currentTile.first.placePiece(l_piece);
			auto l_verif = verifyPiecePlacement(a_currentPostion, l_piece.getPieceZone());
			if (!l_verif)
			{
				l_verif = placePieceOnCurrentTile(l_piece);
			}
			return l_verif;
		}
	}
	else if (a_currentPostion.first > 30 && a_currentPostion.second > 30 && a_currentPostion.first < 60 && a_currentPostion.second < 60)
	{
		l_piece.setPieceZone(Piece::PieceZone::Centre);
		if (m_currentTile.first.getCenter() == Tile::Center::Cloister)
		{
			m_currentTile.first.placePiece(l_piece);
			auto l_verif = verifyPiecePlacement(a_currentPostion, l_piece.getPieceZone());
			if (!l_verif)
			{
				l_verif = placePieceOnCurrentTile(l_piece);
			}
			return l_verif;
		}
	}
	else if (a_currentPostion.first > 60 && a_currentPostion.second > 30 && a_currentPostion.first < 90 && a_currentPostion.second < 60)
	{
		l_piece.setPieceZone(Piece::PieceZone::Right);
		if (m_currentTile.first.getRight() != Tile::Side::Filed)
		{
			m_currentTile.first.placePiece(l_piece);
			auto l_verif = verifyPiecePlacement(a_currentPostion, l_piece.getPieceZone());
			if (!l_verif)
			{
				l_verif = placePieceOnCurrentTile(l_piece);
			}
			return l_verif;
		}
	}
	else if (a_currentPostion.first > 30 && a_currentPostion.second > 60 && a_currentPostion.first < 60 && a_currentPostion.second < 90)
	{
		l_piece.setPieceZone(Piece::PieceZone::Bottom);
		if (m_currentTile.first.getBottom() != Tile::Side::Filed)
		{
			m_currentTile.first.placePiece(l_piece);
			auto l_verif = verifyPiecePlacement(a_currentPostion, l_piece.getPieceZone());
			if (!l_verif)
			{
				l_verif = placePieceOnCurrentTile(l_piece);
			}
			return l_verif;
		}
	}

	return true;
}

auto Game::countPointsOfRoad(const Board::Position &a_position, const Board::Position &a_oldPosition, const Piece::PieceZone a_pieceZone, size_t &a_points, bool &a_isRoadFinished, std::vector<Board::Position> &a_piecePosition) -> void
{
	if (!PlaceTile::areTwoZonesWithRoad(m_board[a_position].value().first))
	{
		switch (a_pieceZone)
		{
		case Piece::PieceZone::Top:
			if (m_board[a_position].value().first.getTop() == Tile::Side::Road)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
					a_piecePosition.push_back(a_position);

				Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
				if (m_board[l_positionTop].has_value() && l_positionTop != a_oldPosition)
				{
					if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionTop].value().first))
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionTop, m_board, Tile::Side::Road) && m_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
							a_piecePosition.push_back(l_positionTop);

						a_points += 2;
					}
					else
					{
						a_points++;
						countPointsOfRoad(l_positionTop, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionTop].has_value())
				{
					a_isRoadFinished = false;
				}
			}
			return;
		case Piece::PieceZone::Bottom:
			if (m_board[a_position].value().first.getBottom() == Tile::Side::Road)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
					a_piecePosition.push_back(a_position);


				Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
				if (m_board[l_positionBottom].has_value() && l_positionBottom != a_oldPosition)
				{
					if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionBottom].value().first))
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionBottom, m_board, Tile::Side::Road) && m_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
							a_piecePosition.push_back(l_positionBottom);

						a_points += 2;
					}
					else
					{
						a_points++;
						countPointsOfRoad(l_positionBottom, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionBottom].has_value())
				{
					a_isRoadFinished = false;
				}
			}
			return;
		case Piece::PieceZone::Left:
			if (m_board[a_position].value().first.getLeft() == Tile::Side::Road)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
					a_piecePosition.push_back(a_position);


				Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
				if (m_board[l_positionLeft].has_value() && l_positionLeft != a_oldPosition)
				{
					if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionLeft].value().first))
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionLeft, m_board, Tile::Side::Road) && m_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
							a_piecePosition.push_back(l_positionLeft);

						a_points += 2;
					}
					else
					{
						a_points++;
						countPointsOfRoad(l_positionLeft, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionLeft].has_value())
				{
					a_isRoadFinished = false;
				}
			}
			return;
		case Piece::PieceZone::Right:
			if (m_board[a_position].value().first.getRight() == Tile::Side::Road)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
					a_piecePosition.push_back(a_position);

				Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
				if (m_board[l_positionRight].has_value() && l_positionRight != a_oldPosition)
				{
					if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionRight].value().first))
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionRight, m_board, Tile::Side::Road) && m_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
							a_piecePosition.push_back(l_positionRight);

						a_points += 2;
					}
					else
					{
						a_points++;
						countPointsOfRoad(l_positionRight, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionRight].has_value())
				{
					a_isRoadFinished = false;
				}
			}
			return;

		default:
			return;
		}
	}

	if (m_board[a_position].value().first.getTop() == Tile::Side::Road)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
			a_piecePosition.push_back(a_position);

		Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
		if (m_board[l_positionTop].has_value() && l_positionTop != a_oldPosition)
		{
			if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionTop].value().first))
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionTop, m_board, Tile::Side::Road) && m_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
					a_piecePosition.push_back(l_positionTop);

				a_points += 2;
			}
			else
			{
				a_points++;
				countPointsOfRoad(l_positionTop, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionTop].has_value())
		{
			a_isRoadFinished = false;
		}
	}

	if (m_board[a_position].value().first.getBottom() == Tile::Side::Road)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
			a_piecePosition.push_back(a_position);


		Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
		if (m_board[l_positionBottom].has_value() && l_positionBottom != a_oldPosition)
		{
			if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionBottom].value().first))
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionBottom, m_board, Tile::Side::Road) && m_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
					a_piecePosition.push_back(l_positionBottom);

				a_points += 2;
			}
			else
			{
				a_points++;
				countPointsOfRoad(l_positionBottom, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionBottom].has_value())
		{
			a_isRoadFinished = false;
		}
	}

	if (m_board[a_position].value().first.getLeft() == Tile::Side::Road)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
			a_piecePosition.push_back(a_position);


		Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
		if (m_board[l_positionLeft].has_value() && l_positionLeft != a_oldPosition)
		{
			if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionLeft].value().first))
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionLeft, m_board, Tile::Side::Road) && m_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
					a_piecePosition.push_back(l_positionLeft);

				a_points += 2;
			}
			else
			{
				a_points++;
				countPointsOfRoad(l_positionLeft, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionLeft].has_value())
		{
			a_isRoadFinished = false;
		}
	}

	if (m_board[a_position].value().first.getRight() == Tile::Side::Road)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::Road) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
			a_piecePosition.push_back(a_position);

		Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
		if (m_board[l_positionRight].has_value() && l_positionRight != a_oldPosition)
		{
			if (!PlaceTile::areTwoZonesWithRoad(m_board[l_positionRight].value().first))
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionRight, m_board, Tile::Side::Road) && m_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
					a_piecePosition.push_back(l_positionRight);

				a_points += 2;
			}
			else
			{
				a_points++;
				countPointsOfRoad(l_positionRight, a_position, a_pieceZone, a_points, a_isRoadFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionRight].has_value())
		{
			a_isRoadFinished = false;
		}
	}

	return;
}

auto Game::countPointsOfCity(const Board::Position &a_position, std::vector<Board::Position> &a_oldPositions, const Piece::PieceZone a_pieceZone, size_t &a_points, bool &l_isCityFinished, std::vector<Board::Position> &a_piecePosition) -> void
{
	if (m_board[a_position].value().first.getCenter() != Tile::Center::City)
	{
		switch (a_pieceZone)
		{
		case Piece::PieceZone::Top:
			if (m_board[a_position].value().first.getTop() == Tile::Side::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
					a_piecePosition.push_back(a_position);

				Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
				if (m_board[l_positionTop].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionTop) == a_oldPositions.end())
				{
					if (m_board[l_positionTop].value().first.getCenter() != Tile::Center::City)
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionTop, m_board, Tile::Side::City) && m_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
							a_piecePosition.push_back(l_positionTop);

						a_points += 2;
					}
					else
					{
						a_points++;
						a_oldPositions.push_back(a_position);
						countPointsOfCity(l_positionTop, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionTop].has_value())
				{
					l_isCityFinished = false;
				}
			}
			return;
		case Piece::PieceZone::Bottom:
			if (m_board[a_position].value().first.getBottom() == Tile::Side::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
					a_piecePosition.push_back(a_position);


				Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
				if (m_board[l_positionBottom].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionBottom) == a_oldPositions.end())
				{
					if (m_board[l_positionBottom].value().first.getCenter() != Tile::Center::City)
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionBottom, m_board, Tile::Side::City) && m_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
							a_piecePosition.push_back(l_positionBottom);

						a_points += 2;
					}
					else
					{
						a_points++;
						a_oldPositions.push_back(a_position);
						countPointsOfCity(l_positionBottom, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionBottom].has_value())
				{
					l_isCityFinished = false;
				}
			}
			return;
		case Piece::PieceZone::Left:
			if (m_board[a_position].value().first.getLeft() == Tile::Side::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
					a_piecePosition.push_back(a_position);


				Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
				if (m_board[l_positionLeft].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionLeft) == a_oldPositions.end())
				{
					if (m_board[l_positionLeft].value().first.getCenter() != Tile::Center::City)
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionLeft, m_board, Tile::Side::City) && m_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
							a_piecePosition.push_back(l_positionLeft);

						a_points += 2;
					}
					else
					{
						a_points++;
						a_oldPositions.push_back(a_position);
						countPointsOfCity(l_positionLeft, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionLeft].has_value())
				{
					l_isCityFinished = false;
				}
			}
			return;
		case Piece::PieceZone::Right:
			if (m_board[a_position].value().first.getRight() == Tile::Side::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
					a_piecePosition.push_back(a_position);

				Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
				if (m_board[l_positionRight].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionRight) == a_oldPositions.end())
				{
					if (m_board[l_positionRight].value().first.getCenter() != Tile::Center::City)
					{
						if (PlaceTile::isThereAnyPieceOnTile(l_positionRight, m_board, Tile::Side::City) && m_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
							a_piecePosition.push_back(l_positionRight);

						a_points += 2;
					}
					else
					{
						a_points++;
						a_oldPositions.push_back(a_position);
						countPointsOfCity(l_positionRight, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
					}
				}
				else if (!m_board[l_positionRight].has_value())
				{
					l_isCityFinished = false;
				}
			}
			return;

		default:
			return;
		}
	}

	if (m_board[a_position].value().first.getTop() == Tile::Side::City)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
			a_piecePosition.push_back(a_position);

		Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
		if (m_board[l_positionTop].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionTop) == a_oldPositions.end())
		{
			if (m_board[l_positionTop].value().first.getCenter() != Tile::Center::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionTop, m_board, Tile::Side::City) && m_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
					a_piecePosition.push_back(l_positionTop);

				a_points++;
			}
			else
			{
				a_oldPositions.push_back(a_position);
				countPointsOfCity(l_positionTop, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionTop].has_value())
		{
			l_isCityFinished = false;
		}
	}

	if (m_board[a_position].value().first.getBottom() == Tile::Side::City)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom)
			a_piecePosition.push_back(a_position);


		Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
		if (m_board[l_positionBottom].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionBottom) == a_oldPositions.end())
		{
			if (m_board[l_positionBottom].value().first.getCenter() != Tile::Center::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionBottom, m_board, Tile::Side::City) && m_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top)
					a_piecePosition.push_back(l_positionBottom);

				a_points++;
			}
			else
			{
				a_oldPositions.push_back(a_position);
				countPointsOfCity(l_positionBottom, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionBottom].has_value())
		{
			l_isCityFinished = false;
		}
	}

	if (m_board[a_position].value().first.getLeft() == Tile::Side::City)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
			a_piecePosition.push_back(a_position);


		Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
		if (m_board[l_positionLeft].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionLeft) == a_oldPositions.end())
		{
			if (m_board[l_positionLeft].value().first.getCenter() != Tile::Center::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionLeft, m_board, Tile::Side::City) && m_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
					a_piecePosition.push_back(l_positionLeft);

				a_points++;
			}
			else
			{
				a_oldPositions.push_back(a_position);
				countPointsOfCity(l_positionLeft, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionLeft].has_value())
		{
			l_isCityFinished = false;
		}
	}

	if (m_board[a_position].value().first.getRight() == Tile::Side::City)
	{
		if (PlaceTile::isThereAnyPieceOnTile(a_position, m_board, Tile::Side::City) && m_board[a_position].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right)
			a_piecePosition.push_back(a_position);

		Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
		if (m_board[l_positionRight].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionRight) == a_oldPositions.end())
		{
			if (m_board[l_positionRight].value().first.getCenter() != Tile::Center::City)
			{
				if (PlaceTile::isThereAnyPieceOnTile(l_positionRight, m_board, Tile::Side::City) && m_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left)
					a_piecePosition.push_back(l_positionRight);

				a_points++;
			}
			else
			{
				a_oldPositions.push_back(a_position);
				countPointsOfCity(l_positionRight, a_oldPositions, a_pieceZone, a_points, l_isCityFinished, a_piecePosition);
			}
		}
		else if (!m_board[l_positionRight].has_value())
		{
			l_isCityFinished = false;
		}
	}

	return;
}

auto Game::countPointsCloister(const Board::Position &a_position, size_t &a_points, bool &l_isCloisterFinished) -> void
{
	auto&[l_row, l_column] = a_position;
	const uint8_t l_noNeighbours = 8;

	const std::array<Board::Position, l_noNeighbours> l_neighbours{ {
			{ static_cast<uint8_t>(l_row - 1) , static_cast<uint8_t>(l_column - 1) },
			{ static_cast<uint8_t>(l_row) , static_cast<uint8_t>(l_column - 1) },
			{ static_cast<uint8_t>(l_row + 1) , static_cast<uint8_t>(l_column - 1) },
			{ static_cast<uint8_t>(l_row + 1) , static_cast<uint8_t>(l_column) },
			{ static_cast<uint8_t>(l_row + 1) , static_cast<uint8_t>(l_column + 1) },
			{ static_cast<uint8_t>(l_row) , static_cast<uint8_t>(l_column + 1) },
			{ static_cast<uint8_t>(l_row - 1) , static_cast<uint8_t>(l_column + 1) },
			{ static_cast<uint8_t>(l_row - 1) , static_cast<uint8_t>(l_column) } } };

	for (auto& neighbour : l_neighbours)
	{
		if (m_board[neighbour].has_value())
			a_points++;
	}

	l_isCloisterFinished = a_points == l_noNeighbours;
}