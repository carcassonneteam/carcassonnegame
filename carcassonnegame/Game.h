#pragma once
#include <string>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include "UnusedTiles.h"
#include "Board.h"
#include "Player.h"

#include <memory>

class Game
{
private:
	using Position = std::pair<int, int>;
public:

	//constructor
	Game() = default;
	Game(const std::string& a_nameFirstPlayer, const std::string& a_nameSecondPlayer);

	//destructor
	~Game() = default;

	//getters
	auto getPoolOfTiles()->UnusedTiles;
	auto getCurrentTile()->UnusedTiles::TileAndName;
	auto getNextTileToPlace()->UnusedTiles::TileAndName;
	auto getCurrentPlayer() ->bool;
	auto getPlayer()const->Player;
	auto getFirstTile()->UnusedTiles::TileAndName;
	auto getPieceOfCurrentTile()->Piece;

	//setters
	//auto setPlayers(Player &player1, Player &player2) noexcept ->void;
	auto setCurrentPlayer(const bool a_player) noexcept ->void;
	auto setCurrentTile(const UnusedTiles::TileAndName & a_tile) ->void;
	auto switchPlayers(bool a_player1) -> void;

	//methods for the logic of the game 
	auto placeTileOnBoard(UnusedTiles::TileAndName && tile, const Position& pos)->bool;
	auto placePieceOnCurrentTile(Piece& a_piece) noexcept->bool;
	auto verifyPiecePlacement(const Position a_currentPostion, const Piece::PieceZone a_pieceZone)noexcept->bool;
	auto placePieceOnTile(const Position a_currentPostion)noexcept->bool;

	//count&add points
	auto countPointsOfRoad(const Board::Position &a_position, const Board::Position &a_oldPosition, const Piece::PieceZone a_pieceZone, size_t &a_points, bool &a_isRoadFinished, std::vector<Board::Position> &a_piecePosition) -> void;
	auto countPointsOfCity(const Board::Position &a_position, std::vector<Board::Position> &a_oldPositions, const Piece::PieceZone a_pieceZone, size_t &a_points, bool &l_isCityFinished, std::vector<Board::Position> &a_piecePosition) -> void;
	auto countPointsCloister(const Board::Position &a_position, size_t &a_points, bool &l_isCloisterFinished) -> void;

	auto addPointsToPlayers(std::vector<Board::Position> &m_piecesToRemoveFromTiles)->void;
	auto addPointsToPlayersAtTheEnd(std::vector<Board::Position> &m_piecesToRemoveFromTiles)->void;

	//supporting methods for count points
	auto methodRoad(std::vector<Board::Position> &m_piecesToRemoveFromTiles)->void;
	auto methodCity(std::vector<Board::Position> &m_piecesToRemoveFromTiles)->void;
	auto methodCloister(std::vector<Board::Position> &m_piecesToRemoveFromTiles)->void;

private:
	UnusedTiles m_poolOfTiles;
	Board m_board;
	std::unique_ptr<Player> m_player1;
	std::unique_ptr<Player> m_player2;

	UnusedTiles::TileAndName m_currentTile;
	Position m_currentPostition;
	std::unique_ptr<Player> m_currentPlayer;
	bool m_playerTurn;
};

