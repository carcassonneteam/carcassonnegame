#pragma once
#include "Board.h"

#include <array>
#include <vector>

/*
Nmaespace Place tile
Has functions for dealing with a tile as:
Place a tile on board, check if a piece can be placed on a tile, check if a tile has any piece and supporting functions
*/

namespace PlaceTile
{
	enum class EdgesBetweenTiles
	{
		TopBottom,
		RightLeft,
		BottomTop,
		LeftRight
	};

	static auto areCompatibleTiles(const std::pair<Tile, std::string>& a_placingTile, const std::pair<Tile, std::string>& a_existingTile, const EdgesBetweenTiles a_edge) -> bool
	{
		switch (a_edge)
		{
		case EdgesBetweenTiles::TopBottom:
			return a_placingTile.first.getTop() == a_existingTile.first.getBottom();
		case EdgesBetweenTiles::RightLeft:
			return a_placingTile.first.getRight() == a_existingTile.first.getLeft();
		case EdgesBetweenTiles::BottomTop:
			return a_placingTile.first.getBottom() == a_existingTile.first.getTop();
		case EdgesBetweenTiles::LeftRight:
			return a_placingTile.first.getLeft() == a_existingTile.first.getRight();
		default:
			return false;
		}
	}

	static auto placeTile(const uint16_t a_row, const uint16_t a_column, std::pair<Tile, std::string>&& a_tile, Board& a_board) -> void
	{

		const Board::Position l_position = { static_cast<uint8_t>(a_row) , static_cast<uint8_t>(a_column) };
		const std::array<Board::Position, 4> l_neighbours{ {
			{ static_cast<uint8_t>(a_row) , static_cast<uint8_t>(a_column - 1) },
			{ static_cast<uint8_t>(a_row + 1) , static_cast<uint8_t>(a_column) },
			{ static_cast<uint8_t>(a_row) , static_cast<uint8_t>(a_column + 1) },
			{ static_cast<uint8_t>(a_row - 1) , static_cast<uint8_t>(a_column) } } };

		auto& l_optionalTile = a_board[l_position];

		if (l_optionalTile)
		{
			throw std::exception("That position is occupied by another piece.");
		}

		bool l_isPlaceableTile = true;
		uint8_t l_noNeighbours = 0;
		for (uint8_t index = 0; index < l_neighbours.size(); ++index)
		{
			if (a_board[l_neighbours[index]] && l_isPlaceableTile)
			{
				l_isPlaceableTile = areCompatibleTiles(a_tile, a_board[l_neighbours[index]].value(), static_cast<EdgesBetweenTiles>(index));
				l_noNeighbours++;
			}
		}

		if (l_isPlaceableTile && l_noNeighbours > 0)
			l_optionalTile = a_tile;

		if (!l_optionalTile)
		{
			throw std::exception("Invalid position");
		}
	}

	static auto areTwoZonesWithRoad(const Tile& a_tile) -> bool
	{
		uint8_t noZonesWithRoad = 0;
		if (a_tile.getTop() == Tile::Side::Road)
			noZonesWithRoad++;
		if (a_tile.getBottom() == Tile::Side::Road)
			noZonesWithRoad++;
		if (a_tile.getLeft() == Tile::Side::Road)
			noZonesWithRoad++;
		if (a_tile.getRight() == Tile::Side::Road)
			noZonesWithRoad++;

		return noZonesWithRoad == 2 ? true : false;
	}

	static auto isThereAnyPieceOnTile(const Board::Position &a_position, Board& a_board, const Tile::Side a_tileSide) -> bool //road
	{
		if (a_board[a_position].value().first.hasPiece() && a_board[a_position].value().first.getSideFromZone(a_board[a_position].value().first.getPiece().getPieceZone()) == a_tileSide)
			return true;
		return false;
	}

	static auto verifyPiecePlacementOnRoad(const Board::Position &a_position, const Board::Position &a_oldPosition, const Piece::PieceZone a_pieceZone, Board& a_board, bool &a_isThereAnyPiece) -> void
	{
		if (isThereAnyPieceOnTile(a_position, a_board, Tile::Side::Road))
		{
			a_isThereAnyPiece = true;
			return;
		}

		if (!areTwoZonesWithRoad(a_board[a_position].value().first))
		{
			switch (a_pieceZone)
			{
			case Piece::PieceZone::Top:
				if (a_board[a_position].value().first.getTop() == Tile::Side::Road)
				{
					Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
					if (a_board[l_positionTop].has_value() && l_positionTop != a_oldPosition)
					{
						if (!areTwoZonesWithRoad(a_board[l_positionTop].value().first))
						{
							a_isThereAnyPiece = a_board[l_positionTop].value().first.hasPiece() && a_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom
								&& a_board[l_positionTop].value().first.getBottom() == Tile::Side::Road;

							if (a_isThereAnyPiece == true)
								return;
							;
						}
						else
							verifyPiecePlacementOnRoad(l_positionTop, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

					}
				}
				return;
			case Piece::PieceZone::Bottom:
				if (a_board[a_position].value().first.getBottom() == Tile::Side::Road)
				{
					Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
					if (a_board[l_positionBottom].has_value() && l_positionBottom != a_oldPosition)
					{
						if (!areTwoZonesWithRoad(a_board[l_positionBottom].value().first))
						{
							a_isThereAnyPiece = a_board[l_positionBottom].value().first.hasPiece() && a_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top
								&& a_board[l_positionBottom].value().first.getTop() == Tile::Side::Road;

							if (a_isThereAnyPiece == true)
								return;
						}
						else
							verifyPiecePlacementOnRoad(l_positionBottom, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

					}
				}
				return;
			case Piece::PieceZone::Left:
				if (a_board[a_position].value().first.getLeft() == Tile::Side::Road)
				{
					Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
					if (a_board[l_positionLeft].has_value() && l_positionLeft != a_oldPosition)
					{
						if (!areTwoZonesWithRoad(a_board[l_positionLeft].value().first))
						{
							a_isThereAnyPiece = a_board[l_positionLeft].value().first.hasPiece() && a_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right
								&& a_board[l_positionLeft].value().first.getRight() == Tile::Side::Road;

							if (a_isThereAnyPiece == true)
								return;
						}
						else
							verifyPiecePlacementOnRoad(l_positionLeft, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

					}
				}
				return;
			case Piece::PieceZone::Right:
				if (a_board[a_position].value().first.getRight() == Tile::Side::Road)
				{
					Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
					if (a_board[l_positionRight].has_value() && l_positionRight != a_oldPosition)
					{
						if (!areTwoZonesWithRoad(a_board[l_positionRight].value().first))
						{
							a_isThereAnyPiece = a_board[l_positionRight].value().first.hasPiece() && a_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left
								&& a_board[l_positionRight].value().first.getLeft() == Tile::Side::Road;

							if (a_isThereAnyPiece == true)
								return;
						}
						else
							verifyPiecePlacementOnRoad(l_positionRight, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

					}
				}
				return;

			default:
				return;
			}
		}

		if (a_board[a_position].value().first.getTop() == Tile::Side::Road)
		{
			Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
			if (a_board[l_positionTop].has_value() && l_positionTop != a_oldPosition)
			{
				if (!areTwoZonesWithRoad(a_board[l_positionTop].value().first))
				{
					a_isThereAnyPiece = a_board[l_positionTop].value().first.hasPiece() && a_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom
						&& a_board[l_positionTop].value().first.getBottom() == Tile::Side::Road;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
					verifyPiecePlacementOnRoad(l_positionTop, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

			}
		}

		if (a_board[a_position].value().first.getBottom() == Tile::Side::Road)
		{
			Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
			if (a_board[l_positionBottom].has_value() && l_positionBottom != a_oldPosition)
			{
				if (!areTwoZonesWithRoad(a_board[l_positionBottom].value().first))
				{
					a_isThereAnyPiece = a_board[l_positionBottom].value().first.hasPiece() && a_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top
						&& a_board[l_positionBottom].value().first.getTop() == Tile::Side::Road;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
					verifyPiecePlacementOnRoad(l_positionBottom, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

			}
		}

		if (a_board[a_position].value().first.getLeft() == Tile::Side::Road)
		{
			Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
			if (a_board[l_positionLeft].has_value() && l_positionLeft != a_oldPosition)
			{
				if (!areTwoZonesWithRoad(a_board[l_positionLeft].value().first))
				{
					a_isThereAnyPiece = a_board[l_positionLeft].value().first.hasPiece() && a_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right
						&&a_board[l_positionLeft].value().first.getRight() == Tile::Side::Road;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
					verifyPiecePlacementOnRoad(l_positionLeft, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

			}
		}

		if (a_board[a_position].value().first.getRight() == Tile::Side::Road)
		{
			Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
			if (a_board[l_positionRight].has_value() && l_positionRight != a_oldPosition)
			{
				if (!areTwoZonesWithRoad(a_board[l_positionRight].value().first))
				{
					a_isThereAnyPiece = a_board[l_positionRight].value().first.hasPiece() && a_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left
						&&a_board[l_positionRight].value().first.getLeft() == Tile::Side::Road;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
					verifyPiecePlacementOnRoad(l_positionRight, a_position, a_pieceZone, a_board, a_isThereAnyPiece);

			}
		}
	}

	static auto verifyPiecePlacementOnCity(const Board::Position &a_position, std::vector<Board::Position> &a_oldPositions, const Piece::PieceZone a_pieceZone, Board& a_board, bool &a_isThereAnyPiece) -> void
	{
		if (isThereAnyPieceOnTile(a_position, a_board, Tile::Side::City))
		{
			a_isThereAnyPiece = true;
			return;
		}

		if (a_board[a_position].value().first.getCenter() != Tile::Center::City)
		{
			switch (a_pieceZone)
			{
			case Piece::PieceZone::Top:
				if (a_board[a_position].value().first.getTop() == Tile::Side::City)
				{
					Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
					if (a_board[l_positionTop].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionTop) == a_oldPositions.end())
					{
						if (a_board[l_positionTop].value().first.getCenter() != Tile::Center::City)
						{
							a_isThereAnyPiece = a_board[l_positionTop].value().first.hasPiece() && a_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom
								&& a_board[l_positionTop].value().first.getBottom() == Tile::Side::City;
							
							if(a_isThereAnyPiece == true)
								return;
						}
						else
						{
							a_oldPositions.push_back(a_position);
							verifyPiecePlacementOnCity(l_positionTop, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
						}

					}
				}
				return;
			case Piece::PieceZone::Bottom:
				if (a_board[a_position].value().first.getBottom() == Tile::Side::City)
				{
					Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
					if (a_board[l_positionBottom].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionBottom) == a_oldPositions.end())
					{
						if (a_board[l_positionBottom].value().first.getCenter() != Tile::Center::City)
						{
							a_isThereAnyPiece = a_board[l_positionBottom].value().first.hasPiece() && a_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top
								&& a_board[l_positionBottom].value().first.getTop() == Tile::Side::City;
							
							if (a_isThereAnyPiece == true)
								return;
						}
						else
						{
							a_oldPositions.push_back(a_position);
							verifyPiecePlacementOnCity(l_positionBottom, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
						}

					}
				}
				return;
			case Piece::PieceZone::Left:
				if (a_board[a_position].value().first.getLeft() == Tile::Side::City)
				{
					Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
					if (a_board[l_positionLeft].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionLeft) == a_oldPositions.end())
					{
						if (a_board[l_positionLeft].value().first.getCenter() != Tile::Center::City)
						{
							a_isThereAnyPiece = a_board[l_positionLeft].value().first.hasPiece() && a_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right
								&& a_board[l_positionLeft].value().first.getRight() == Tile::Side::City;

							if (a_isThereAnyPiece == true)
								return;
						}
						else
						{
							a_oldPositions.push_back(a_position);
							verifyPiecePlacementOnCity(l_positionLeft, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
						}

					}
				}
				return;
			case Piece::PieceZone::Right:
				if (a_board[a_position].value().first.getRight() == Tile::Side::City)
				{
					Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
					if (a_board[l_positionRight].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionRight) == a_oldPositions.end())
					{
						if (a_board[l_positionRight].value().first.getCenter() != Tile::Center::City)
						{
							a_isThereAnyPiece = a_board[l_positionRight].value().first.hasPiece() && a_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left
								&& a_board[l_positionRight].value().first.getLeft() == Tile::Side::City;

							if (a_isThereAnyPiece == true)
								return;
						}
						else
						{
							a_oldPositions.push_back(a_position);
							verifyPiecePlacementOnCity(l_positionRight, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
						}

					}
				}
				return;

			default:
				return;
			}
		}

		if (a_board[a_position].value().first.getTop() == Tile::Side::City)
		{
			Board::Position l_positionTop = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second - 1) };
			if (a_board[l_positionTop].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionTop) == a_oldPositions.end())
			{
				if (a_board[l_positionTop].value().first.getCenter() != Tile::Center::City)
				{
					a_isThereAnyPiece = a_board[l_positionTop].value().first.hasPiece() && a_board[l_positionTop].value().first.getPiece().getPieceZone() == Piece::PieceZone::Bottom
						&& a_board[l_positionTop].value().first.getBottom() == Tile::Side::City;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
				{
					a_oldPositions.push_back(a_position);
					verifyPiecePlacementOnCity(l_positionTop, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
				}

			}
		}

		if (a_board[a_position].value().first.getBottom() == Tile::Side::City)
		{
			Board::Position l_positionBottom = { static_cast<uint8_t>(a_position.first), static_cast<uint8_t>(a_position.second + 1) };
			if (a_board[l_positionBottom].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionBottom) == a_oldPositions.end())
			{
				if (a_board[l_positionBottom].value().first.getCenter() != Tile::Center::City)
				{
					a_isThereAnyPiece = a_board[l_positionBottom].value().first.hasPiece() && a_board[l_positionBottom].value().first.getPiece().getPieceZone() == Piece::PieceZone::Top
						&& a_board[l_positionBottom].value().first.getTop() == Tile::Side::City;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
				{
					a_oldPositions.push_back(a_position);
					verifyPiecePlacementOnCity(l_positionBottom, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
				}
			}
		}

		if (a_board[a_position].value().first.getLeft() == Tile::Side::City)
		{
			Board::Position l_positionLeft = { static_cast<uint8_t>(a_position.first - 1), static_cast<uint8_t>(a_position.second) };
			if (a_board[l_positionLeft].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionLeft) == a_oldPositions.end())
			{
				if (a_board[l_positionLeft].value().first.getCenter() != Tile::Center::City)
				{
					a_isThereAnyPiece = a_board[l_positionLeft].value().first.hasPiece() && a_board[l_positionLeft].value().first.getPiece().getPieceZone() == Piece::PieceZone::Right
						&&a_board[l_positionLeft].value().first.getRight() == Tile::Side::City;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
				{
					a_oldPositions.push_back(a_position);
					verifyPiecePlacementOnCity(l_positionLeft, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
				}
			}
		}

		if (a_board[a_position].value().first.getRight() == Tile::Side::City)
		{
			Board::Position l_positionRight = { static_cast<uint8_t>(a_position.first + 1), static_cast<uint8_t>(a_position.second) };
			if (a_board[l_positionRight].has_value() && std::find(a_oldPositions.begin(), a_oldPositions.end(), l_positionRight) == a_oldPositions.end())
			{
				if (a_board[l_positionRight].value().first.getCenter() != Tile::Center::City)
				{
					a_isThereAnyPiece = a_board[l_positionRight].value().first.hasPiece() && a_board[l_positionRight].value().first.getPiece().getPieceZone() == Piece::PieceZone::Left
						&&a_board[l_positionRight].value().first.getLeft() == Tile::Side::City;

					if (a_isThereAnyPiece == true)
						return;
				}
				else
				{
					a_oldPositions.push_back(a_position);
					verifyPiecePlacementOnCity(l_positionRight, a_oldPositions, a_pieceZone, a_board, a_isThereAnyPiece);
				}
			}
		}
	}

}