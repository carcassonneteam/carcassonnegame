#include "Menu.h"
#include <iostream>

Menu::Menu(const float a_width, const float a_height)
{
	m_selectedItemIndex = 0;

	if (!m_font.loadFromFile("ALGER.ttf"))
	{
		std::cout << "Can't load the font text for the GUI menu. ";
	}

	setAtributesForShapes(m_shapes[0], sf::Vector2f(300, 40), sf::Color(255, 220, 0, 255), sf::Color(100, 0, 0, 255), 5, sf::Vector2f(150, a_height / (sc_maxNumberOfItems + 2) * 1));
	setAtributesForShapes(m_shapes[1], sf::Vector2f(300, 40), sf::Color(255, 171, 0, 255), sf::Color::Black, 2, sf::Vector2f(150, a_height / (sc_maxNumberOfItems + 2) * 2));
	setAtributesForShapes(m_shapes[2], sf::Vector2f(300, 40), sf::Color(255, 171, 0, 255), sf::Color::Black, 2, sf::Vector2f(150, a_height / (sc_maxNumberOfItems + 2) * 3));

	setAtributesForText(m_menu[0], m_font, sf::Color(100, 0, 0, 255), "START NEW GAME", sf::Vector2f(170, a_height / (sc_maxNumberOfItems + 2) * 1));
	setAtributesForText(m_menu[1], m_font, sf::Color::Black, "Sound ON/OFF", sf::Vector2f(200, a_height / (sc_maxNumberOfItems + 2) * 2));
	setAtributesForText(m_menu[2], m_font, sf::Color::Black, "EXIT GAME", sf::Vector2f(230, a_height / (sc_maxNumberOfItems + 2) * 3));
}

auto Menu::Run(sf::RenderWindow & a_window, bool & a_musicPlaying) ->int
{
	bool l_running = true;
	sf::Music l_music;
	if (!l_music.openFromFile("BackgroundMusic.ogg"))
	{
		std::cout << "file not found!" << std::endl;
	}
	l_music.play();
	l_music.setVolume(35);
	while (l_running)
	{
		sf::Event l_myEvent;

		while (a_window.pollEvent(l_myEvent))
		{
			switch (l_myEvent.type)
			{
			case sf::Event::Closed:
				a_window.close();
				return -1;
				break;
			case sf::Event::KeyReleased:
				switch (l_myEvent.key.code)
				{
				case sf::Keyboard::Up:
					moveUp();
					break;
				case sf::Keyboard::Down:
					moveDown();
					break;
				case sf::Keyboard::Return:
					switch (getPressedItem())
					{
					case 0:
						return 1;
						break;
					case 1:
						if (a_musicPlaying == true)
						{
							l_music.pause();
							a_musicPlaying = false;
						}
						else
						{
							l_music.play();
							a_musicPlaying = true;
						}
						break;
					case 2:
						a_window.close();
						return -1;
						break;
					default:
						return -1;
						break;
					}
					break;
				}
				break;
			}
		}
		a_window.clear();
		draw(a_window);
		a_window.display();
	}
	return -1;
}

auto Menu::draw(sf::RenderWindow & a_window) -> void
{
	sf::Sprite l_sprite;
	sf::Texture l_texture;

	if (!l_texture.loadFromFile("carcassonneGUI.jpg"))
	{
		std::cout << "Photo not found!" << std::endl;
	}
	l_sprite.setTexture(l_texture);
	a_window.draw(l_sprite);

	for (auto&& l_shape : m_shapes)
		a_window.draw(l_shape);

	for (auto&& l_menu : m_menu)
		a_window.draw(l_menu);
}

auto Menu::moveUp() -> void
{
	if (m_selectedItemIndex - 1 >= 0)
	{
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 171, 0, 255), sf::Color::Black, 2);
		m_menu[m_selectedItemIndex].setFillColor(sf::Color::Black);
		m_selectedItemIndex--;
		m_menu[m_selectedItemIndex].setFillColor(sf::Color(100, 0, 0, 255));
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 220, 0, 255), sf::Color(100, 0, 0, 255), 5);
	}
	else
	{
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 171, 0, 255), sf::Color::Black, 2);
		m_menu[m_selectedItemIndex].setFillColor(sf::Color::Black);
		m_selectedItemIndex = sc_maxNumberOfItems - 1;
		m_menu[m_selectedItemIndex].setFillColor(sf::Color(100, 0, 0, 255));
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 220, 0, 255), sf::Color(100, 0, 0, 255), 5);
	}
}

auto Menu::moveDown() -> void
{
	if (m_selectedItemIndex + 1 < sc_maxNumberOfItems)
	{
		m_menu[m_selectedItemIndex].setFillColor(sf::Color::Black);
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 171, 0, 255), sf::Color::Black, 2);
		m_selectedItemIndex++;
		m_menu[m_selectedItemIndex].setFillColor(sf::Color(100, 0, 0, 255));
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 220, 0, 255), sf::Color(100, 0, 0, 255), 5);
	}
	else
	{
		m_menu[m_selectedItemIndex].setFillColor(sf::Color::Black);
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 171, 0, 255), sf::Color::Black, 2);
		m_selectedItemIndex = 0;
		m_menu[m_selectedItemIndex].setFillColor(sf::Color(100, 0, 0, 255));
		setAtributesForShapes(m_shapes[m_selectedItemIndex], sf::Color(255, 220, 0, 255), sf::Color(100, 0, 0, 255), 5);
	}
}

auto Menu::getPressedItem() -> int
{
	return m_selectedItemIndex;
}

auto Menu::setAtributesForShapes(sf::RectangleShape & a_shape, const sf::Vector2f & a_size, const sf::Color & a_fillColor, const sf::Color & a_outlineColor, float a_outlineThickness, const sf::Vector2f & a_position) -> void
{
	setAtributesForShapes(a_shape, a_fillColor, a_outlineColor, a_outlineThickness);
	a_shape.setSize(a_size);
	a_shape.setPosition(a_position);
}

auto Menu::setAtributesForShapes(sf::RectangleShape & a_shape, const sf::Color & a_fillColor, const sf::Color & a_outlineColor, float a_outlineThickness) -> void
{
	a_shape.setFillColor(a_fillColor);
	a_shape.setOutlineColor(a_outlineColor);
	a_shape.setOutlineThickness(a_outlineThickness);
}

auto Menu::setAtributesForText(sf::Text & a_elementMenu, const sf::Font & myFont, const sf::Color & a_color, const sf::String & a_string, const sf::Vector2f & a_position) -> void
{
	a_elementMenu.setFont(myFont);
	a_elementMenu.setFillColor(a_color);
	a_elementMenu.setString(a_string);
	a_elementMenu.setString(a_string);
	a_elementMenu.setPosition(a_position);
}
