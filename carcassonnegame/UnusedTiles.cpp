#include "UnusedTiles.h"


UnusedTiles::UnusedTiles() :
	m_keyPool(0)
{
	generateTiles();
}

auto UnusedTiles::generateTiles() noexcept ->void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Generating tiles...", Logger::Level::Info);

	//generateOnly the start tile
	generateStartTile();
	//generate all the tiles
	generateTileType_a();
	generateTileType_b();
	generateTileType_c();
	generateTileType_c1();
	generateTileType_d();
	generateTileType_d1();
	generateTileType_e();
	generateTileType_f();
	generateTileType_f1();
	generateTileType_g();
	generateTileType_h();
	generateTileType_h1();
	generateTileType_i();
	generateTileType_i1();
	generateTileType_j();
	generateTileType_k();
	generateTileType_l();
	generateTileType_m();
	generateTileType_n();
	generateTileType_o();
	generateTileType_p();
	generateTileType_q();
	generateTileType_r();
	generateTileType_s();

	of.close();
}

auto UnusedTiles::generateSpecificTile(
	const std::string & a_tileName,
	size_t a_numberOfTimes,
	const Tile::Side a_top,
	const Tile::Side a_right,
	const Tile::Side a_bottom,
	const Tile::Side a_left,
	const Tile::Side a_topRight,
	const Tile::Side a_topLeft,
	const Tile::Side a_bottomRight,
	const Tile::Side a_bottomLeft,
	const Tile::Center a_center,
	const Tile::BonusPoint a_bonusPoint
) noexcept -> void
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	const std::string l_tileType = a_tileName;
	std::string l_generatingTile = "Generate tile type ";
	std::string l_endOfString = "...";
	l_generatingTile.append(l_tileType);
	l_generatingTile.append(l_endOfString);
	log.log(l_generatingTile, Logger::Level::Info);
	const Tile l_tile(
		a_top,
		a_right,
		a_bottom,
		a_left,
		a_topRight,
		a_topLeft,
		a_bottomRight,
		a_bottomLeft,
		a_center,
		a_bonusPoint
	);

	while(a_numberOfTimes--)
		m_pool[m_keyPool++] = std::make_pair(l_tile, l_tileType);
	of.close();
}

auto UnusedTiles::getTile() -> TileAndName
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Extracting tile...", Logger::Level::Info);
	std::random_device rand;
	std::mt19937 mt(rand());
	std::uniform_int_distribution<size_t> random(0, mc_totalNumberOfPieces - 1);
	if (m_keyPool--)
	{
		while (true)
		{
			size_t l_randomPozistion = random(mt);

			auto l_node = m_pool.extract(l_randomPozistion);
			if (l_node)
			{
				log.log("Tile extracted succesful...", Logger::Level::Info);
				return std::move(l_node.mapped());
			}
			else
				continue;
		}	
	}
	else
	{
		m_keyPool = 0;
		Tile l_tile;
		std::string l_tileType = "x";
		TileAndName l_returnTile = std::make_pair(l_tile, l_tileType);
		return l_returnTile;//return null tile/game finishes when we are out of tiles
	}
	/*OR*/
	/*std::random_device rand;
	std::mt19937 mt(rand());
	std::uniform_int_distribution<size_t> random(0, m_pool.size() - 1);
	auto random_it = std::next(std::begin(m_pool), random(mt));*/
}

auto UnusedTiles::getStartTile() -> TileAndName
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Extracting start tile...", Logger::Level::Info);
	of.close();

	return std::move(m_startTile);
}

auto UnusedTiles::getNumberOfRemaningTiles() -> size_t
{
	return this->m_keyPool;
}

auto UnusedTiles::generateStartTile() noexcept -> void
{
	std::string l_tileType = "s";
	Tile l_startTile(
		Tile::Side::City,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::None
	);
	m_startTile = std::make_pair(l_startTile, l_tileType);
}

auto UnusedTiles::generateTileType_a() noexcept -> void
{
	std::string l_tileType = "a";
	generateSpecificTile(
		l_tileType,
		1,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Center::City,
		Tile::BonusPoint::True
	);
}

auto UnusedTiles::generateTileType_b() noexcept -> void
{
	std::string l_tileType = "b";
	generateSpecificTile(
		l_tileType,
		1,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::Intersection
	);
}

auto UnusedTiles::generateTileType_c() noexcept -> void
{
	std::string l_tileType = "c";
	generateSpecificTile(
		l_tileType,
		3,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Center::City
	);
}

auto UnusedTiles::generateTileType_c1() noexcept -> void
{
	std::string l_tileType = "c1";
	generateSpecificTile(
		l_tileType,
		1,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::City, 
		Tile::Side::City, 
		Tile::Side::City, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Center::City,
		Tile::BonusPoint::True
	);
}

auto UnusedTiles::generateTileType_d() noexcept -> void
{
	std::string l_tileType = "d";
	generateSpecificTile(
		l_tileType,
		1,
		Tile::Side::City,
		Tile::Side::City, 
		Tile::Side::Road,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::City
	);
}

auto UnusedTiles::generateTileType_d1() noexcept -> void
{
	 std::string l_tileType = "d1";
	 generateSpecificTile(
		 l_tileType,
		 2, 
		 Tile::Side::City,
		 Tile::Side::City, 
		 Tile::Side::Road, 
		 Tile::Side::City, 
		 Tile::Side::City, 
		 Tile::Side::City, 
		 Tile::Side::Filed, 
		 Tile::Side::Filed, 
		 Tile::Center::City,
		 Tile::BonusPoint::True
	 );
}

auto UnusedTiles::generateTileType_e() noexcept -> void
{
	std::string l_tileType = "e";
	generateSpecificTile(
		l_tileType,
		4,
		Tile::Side::Filed,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::Intersection
	);
}

auto UnusedTiles::generateTileType_f() noexcept -> void
{
	std::string l_tileType = "f";
	generateSpecificTile(
		l_tileType,
		1,
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::City
	);
}

auto UnusedTiles::generateTileType_f1() noexcept -> void
{
	std::string l_tileType = "f1";
	generateSpecificTile(
		l_tileType,
		2,
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::City,
		Tile::BonusPoint::True
	);
}

auto UnusedTiles::generateTileType_g() noexcept -> void
{
	std::string l_tileType = "g";
	generateSpecificTile(
		l_tileType,
		8,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Road,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_h() noexcept -> void
{
	std::string l_tileType = "h";
	generateSpecificTile(
		l_tileType,
		3,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::City, 
		Tile::Side::Filed,
		Tile::Side::City, 
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::City
	);
}

auto UnusedTiles::generateTileType_h1() noexcept -> void
{
	std::string l_tileType = "h1";
	generateSpecificTile(
		l_tileType,
		2, 
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::City, 
		Tile::Side::Filed, 
		Tile::Side::City, 
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::City, 
		Tile::BonusPoint::True
	);
}

auto UnusedTiles::generateTileType_i() noexcept -> void
{
	std::string l_tileType = "i";
	generateSpecificTile(
		l_tileType,
		3, 
		Tile::Side::City,
		Tile::Side::Road,
		Tile::Side::Road, 
		Tile::Side::City,
		Tile::Side::Filed, 
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::City
	);
}

auto UnusedTiles::generateTileType_i1() noexcept -> void
{
	std::string l_tileType = "i1";
	generateSpecificTile(
		l_tileType,
		2,
		Tile::Side::City,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::City,
		Tile::BonusPoint::True
	);
}

auto UnusedTiles::generateTileType_j() noexcept -> void
{
	std::string l_tileType = "j";
	generateSpecificTile(
		l_tileType,
		9, 
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Road, 
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_k() noexcept -> void
{
	std::string l_tileType = "k";
	generateSpecificTile(
		l_tileType,
		2,
		Tile::Side::City,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_l() noexcept -> void
{
	std::string l_tileType = "l";
	generateSpecificTile(
		l_tileType,
		3, 
		Tile::Side::Filed,
		Tile::Side::City,
		Tile::Side::Filed, 
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_m() noexcept -> void
{
	std::string l_tileType = "m";
	generateSpecificTile(
		l_tileType,
		2,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Road,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Center::Cloister
	);
}

auto UnusedTiles::generateTileType_n() noexcept -> void
{
	std::string l_tileType = "n";
	generateSpecificTile(
		l_tileType,
		4,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Center::Cloister
	);
}

auto UnusedTiles::generateTileType_o() noexcept -> void
{
	std::string l_tileType = "o";
	generateSpecificTile(
		l_tileType,
		5,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_p() noexcept -> void
{
	std::string l_tileType = "p";
	generateSpecificTile(
		l_tileType,
		3,
		Tile::Side::City, 
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_q() noexcept -> void
{
	std::string l_tileType = "q";
	generateSpecificTile(
		l_tileType,
		3,
		Tile::Side::City,
		Tile::Side::Filed,
		Tile::Side::Road, 
		Tile::Side::Road, 
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Center::None
	);
}

auto UnusedTiles::generateTileType_r() noexcept -> void
{
	std::string l_tileType = "r";
	generateSpecificTile(
		l_tileType,
		3,
		Tile::Side::City,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Center::Intersection
	);
}

auto UnusedTiles::generateTileType_s() noexcept -> void
{
	std::string l_tileType = "s";
	generateSpecificTile(
		l_tileType,
		3, 
		Tile::Side::City,
		Tile::Side::Road,
		Tile::Side::Filed,
		Tile::Side::Road, 
		Tile::Side::Filed,
		Tile::Side::Filed, 
		Tile::Side::Filed,
		Tile::Side::Filed,
		Tile::Center::None
	);
}