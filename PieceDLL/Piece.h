#pragma once

#include "../Logging/Logging.h"

#include <cstdint>
#include <iostream>
#include <fstream>


#ifdef PIECEDLL_EXPORTS
#define PIECE_API __declspec(dllexport)
#else
#define PIECE_API __declspec(dllimport)
#endif


/*
Piece class
Holds all the requierd information for a piece
*/
class PIECE_API Piece
{

public:

	//This enum holds all the posible zone where a pice can be
	enum class PieceZone : uint8_t
	{
		TopLeft,
		Top,
		TopRight,
		Right,
		BottomRight,
		Bottom,
		BottomLeft,
		Left,
		Centre,
		None
	};
public:
	//constructors
	Piece();
	Piece(PieceZone pieceZone);

	//destructor
	~Piece() = default;

public:
	//getter
	PieceZone getPieceZone() const noexcept;

	//seter
	void setPieceZone(PieceZone pieceZone) noexcept;

public:
	PieceZone m_pieceZone;
};