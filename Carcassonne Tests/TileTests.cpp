#include "stdafx.h"
#include "CppUnitTest.h"
#include "../PieceDLL/Piece.h"
#include "Tile.h"
#include "Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(TileTests)
	{
	public:
		TEST_METHOD(CheckDefaultConstructor)
		{
			Tile tile;
			Assert::IsTrue(tile.getTopLeft() == Tile::Side::None);
			Assert::IsTrue(tile.getTop() == Tile::Side::None);
			Assert::IsTrue(tile.getTopRight() == Tile::Side::None);
			Assert::IsTrue(tile.getRight() == Tile::Side::None);
			Assert::IsTrue(tile.getBottomRight() == Tile::Side::None);
			Assert::IsTrue(tile.getBottom() == Tile::Side::None);
			Assert::IsTrue(tile.getBottomLeft() == Tile::Side::None);
			Assert::IsTrue(tile.getRight() == Tile::Side::None);
			Assert::IsTrue(tile.getCenter() == Tile::Center::None);
			Assert::IsTrue(tile.getBonusPoint() == Tile::BonusPoint::None);

		}

		TEST_METHOD(CheckConstructorForGeneratingTiles)
		{
			Tile tile(Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Center::None, Tile::BonusPoint::True);
			Assert::IsTrue(tile.getTopLeft() == Tile::Side::City);
			Assert::IsTrue(tile.getTop() == Tile::Side::City);
			Assert::IsTrue(tile.getTopRight() == Tile::Side::City);
			Assert::IsTrue(tile.getRight() == Tile::Side::City);
			Assert::IsTrue(tile.getBottomRight() == Tile::Side::City);
			Assert::IsTrue(tile.getBottom() == Tile::Side::City);
			Assert::IsTrue(tile.getBottomLeft() == Tile::Side::City);
			Assert::IsTrue(tile.getLeft() == Tile::Side::City);
			Assert::IsTrue(tile.getCenter() == Tile::Center::None);
			Assert::IsTrue(tile.getBonusPoint() == Tile::BonusPoint::True);
		}

		TEST_METHOD(CheckConstructorForSpecialPieces)
		{
			Tile tile;
			tile.setbottom(Tile::Side::City);
			tile.setbottomLeft(Tile::Side::City);
			tile.setbottomRight(Tile::Side::City);
			tile.setLeft(Tile::Side::City);
			tile.setRight(Tile::Side::City);
			tile.setTop(Tile::Side::City);
			tile.setTopLeft(Tile::Side::City);
			tile.setTopRight(Tile::Side::City);
			tile.setCenter(Tile::Center::None);
			tile.setBonusPoint(Tile::BonusPoint::True);

			Assert::IsTrue(tile.getBottom() == Tile::Side::City);
			Assert::IsTrue(tile.getBottomLeft() == Tile::Side::City);
			Assert::IsTrue(tile.getBottomRight() == Tile::Side::City);
			Assert::IsTrue(tile.getLeft() == Tile::Side::City);
			Assert::IsTrue(tile.getRight() == Tile::Side::City);
			Assert::IsTrue(tile.getTop() == Tile::Side::City);
			Assert::IsTrue(tile.getTopLeft() == Tile::Side::City);
			Assert::IsTrue(tile.getTopRight() == Tile::Side::City);
			Assert::IsTrue(tile.getCenter() == Tile::Center::None);
			Assert::IsTrue(tile.getBonusPoint() == Tile::BonusPoint::True);
		}

		TEST_METHOD(CheckIfTileHasPiece)
		{
			Tile tile(Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Side::City, Tile::Center::None, Tile::BonusPoint::True);
			Assert::IsTrue(tile.hasPiece() == false);//should not have a piece
		}

		TEST_METHOD(CheckPlacePieceMethod)
		{
			Piece piece;
			Tile tile(Tile::Side::City, Tile::Side::Road, Tile::Side::Road, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::Intersection);
			piece.setPieceZone(Piece::PieceZone::Bottom);

			tile.placePiece(piece);

			Assert::IsTrue(tile.hasPiece() == true);
			Assert::IsTrue(tile.getPiece().getPieceZone() == piece.getPieceZone());
		}

		TEST_METHOD(CheckIfRoateTileToRightMethodWorks)
		{
			Tile tileToRotate(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			Tile tileRotated(Tile::Side::Road, Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);

			tileToRotate.rotateTileToRight();

			Assert::IsTrue(tileToRotate.getTop() == tileRotated.getTop());
			Assert::IsTrue(tileToRotate.getRight() == tileRotated.getRight());
			Assert::IsTrue(tileToRotate.getBottom() == tileRotated.getBottom());
			Assert::IsTrue(tileToRotate.getLeft() == tileRotated.getLeft());
		}

		TEST_METHOD(CheckIfOperatorAssignForCopyWorksAsIntended)
		{
			Tile tile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			Tile copyTile;

			copyTile = tile;

			Assert::IsTrue(tile.getTop() == copyTile.getTop());
			Assert::IsTrue(tile.getRight() == copyTile.getRight());
			Assert::IsTrue(tile.getBottom() == copyTile.getBottom());
			Assert::IsTrue(tile.getLeft() == copyTile.getLeft());
		}

		TEST_METHOD(CheckIfOperatorAssignForMoveWorksAsIntended)
		{
			Tile tile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			Tile copyTile;
			Tile moveTile;

			copyTile = tile;
			moveTile = std::move(tile);

			Assert::IsTrue(moveTile.getTop() == copyTile.getTop());
			Assert::IsTrue(moveTile.getRight() == copyTile.getRight());
			Assert::IsTrue(moveTile.getBottom() == copyTile.getBottom());
			Assert::IsTrue(moveTile.getLeft() == copyTile.getLeft());
		}
	};
}