#include "stdafx.h"
#include "CppUnitTest.h"
#include "../PieceDLL/Piece.h" 

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(PieceCheckTest)
		{
			Piece piece;
			piece.setPieceZone(Piece::PieceZone::Bottom);
			Assert::IsTrue(piece.getPieceZone() == Piece::PieceZone::Bottom);
		}

		TEST_METHOD(CheckPieceDefaultConstructor)
		{
			Piece piece;
			Assert::IsTrue(piece.getPieceZone() == Piece::PieceZone::None);
		}

		TEST_METHOD(CheckPieceConstructorWithOneParameter)
		{
			Piece piece(Piece::PieceZone::Bottom);
			Assert::IsTrue(piece.getPieceZone() == Piece::PieceZone::Bottom);

		}

	};
}