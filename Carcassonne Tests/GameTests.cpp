#include "stdafx.h"
#include "CppUnitTest.h"
#include "UnusedTiles.h"
#include "Tile.h"
#include "Board.h"
#include "../PieceDLL/Piece.h"
#include "Game.h" 

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(GameTests)
	{
	public:

		//TEST_METHOD(CheckIfReturnsRightCurrentPlayer)
		//{
		//	Game game;
		//	bool player;
		//	Player player1, player2, currentPlayer;
		//	player1.setPlayerName("Bob");
		//	player2.setPlayerName("Mark");
		//	game.setPlayers(player1, player2);
		//	player = false;
		//	game.switchPlayers(player);
		//	Assert::IsTrue(game.getPlayer() == player2);
		//}

		TEST_METHOD(CheckTheInitializeOfStartTile)
		{
			Game game;
			UnusedTiles unusedTiles;
			auto tile = game.getFirstTile();
			Tile startTile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);

			//check if returns the correct value
			Assert::IsTrue(tile.first.getTop() == startTile.getTop());
			Assert::IsTrue(tile.first.getLeft() == startTile.getLeft());
			Assert::IsTrue(tile.first.getRight() == startTile.getRight());
			Assert::IsTrue(tile.first.getBottom() == startTile.getBottom());
		}

		TEST_METHOD(CheckPlacementOfTileOnBoard)
		{
			Game game;
			game.getFirstTile();
			UnusedTiles unusedTiles;
			std::string nameString = "s";
			Tile tile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			bool ok = game.placeTileOnBoard(std::move(std::make_pair(tile, nameString)), { 71, 72 });

			Assert::IsTrue(ok);

			//try to place another time, should return false
			ok = game.placeTileOnBoard(std::move(std::make_pair(tile, nameString)), { 71, 72 });
			Assert::IsFalse(ok);
		}

		TEST_METHOD(CheckPlacementOfPieceOnCurrentTile)
		{
			Game game;
			game.getFirstTile();
			UnusedTiles unusedTiles;
			std::string nameString = "s";
			Tile tile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);

			if (game.placeTileOnBoard(std::move(std::make_pair(tile, nameString)), { 71, 72 }) == false)
			{
				//simulate graphic press, this reprezents topRight
				game.placePieceOnTile({ 15, 15 });

				Assert::IsTrue(game.getPieceOfCurrentTile().getPieceZone() == Piece::PieceZone::TopLeft);
			}

		}
	};
}