#include "stdafx.h"
#include "CppUnitTest.h"
#include "UnusedTiles.h" 

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(UnusedTilesTests)
	{
	public:

		TEST_METHOD(CheckTheGenerateOfUnusedTiles)
		{
			UnusedTiles unusedTile;
			for (size_t index = 0; index < 71; ++index)
			{
				auto tile = unusedTile.getTile();
				Assert::IsTrue(tile.first.getTop() != Tile::Side::None);
				Assert::IsTrue(tile.first.getBottom() != Tile::Side::None);
				Assert::IsTrue(tile.first.getLeft() != Tile::Side::None);
				Assert::IsTrue(tile.first.getRight() != Tile::Side::None);
				Assert::IsTrue(tile.first.getCenter() != Tile::Center::None || tile.first.getCenter() == Tile::Center::None);
				Assert::IsTrue(tile.first.getBonusPoint() != Tile::BonusPoint::None || tile.first.getBonusPoint() != Tile::BonusPoint::True);
			}
		}

		TEST_METHOD(CheckStartTileMethod)
		{
			UnusedTiles unusedTiles;
			auto tile = unusedTiles.getStartTile();
			Assert::IsTrue(tile.first.getTop() == Tile::Side::City);
			Assert::IsTrue(tile.first.getLeft() == Tile::Side::Road);
			Assert::IsTrue(tile.first.getRight() == Tile::Side::Road);
			Assert::IsTrue(tile.first.getBottom() == Tile::Side::Filed);
		}

		TEST_METHOD(CheckGetTileMethod)
		{
			UnusedTiles unusedTiles;
			auto tile = unusedTiles.getTile();

			Assert::IsTrue(tile.first.getTop() != Tile::Side::None);
			Assert::IsTrue(tile.first.getLeft() != Tile::Side::None);
			Assert::IsTrue(tile.first.getRight() != Tile::Side::None);
			Assert::IsTrue(tile.first.getBottom() != Tile::Side::None);
		}

	};
}