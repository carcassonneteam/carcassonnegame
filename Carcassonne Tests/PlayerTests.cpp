#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h" 
#include "UnusedTiles.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(CheckIfTileIsAddedOnBoard)
		{
			Board::Position position;
			Player player;
			Board board;
			Tile startTile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			Tile playerTile(Tile::Side::City, Tile::Side::Road, Tile::Side::Road, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::Intersection);
			std::string nameOfTile = "s";

			auto startTileWithName = std::make_pair(startTile, nameOfTile);
			auto playerTileWithName = std::make_pair(playerTile, nameOfTile);
			board.setStartTile(std::move(startTileWithName));
			position.first = 71;
			position.second = 72;
			player.addTileOnBoard(std::move(playerTileWithName), position, board);

			Assert::IsTrue(board[position].value().first.getTop() == playerTile.getTop());
			Assert::IsTrue(board[position].value().first.getRight() == playerTile.getRight());
			Assert::IsTrue(board[position].value().first.getBottom() == playerTile.getBottom());
			Assert::IsTrue(board[position].value().first.getLeft() == playerTile.getLeft());

		}

		TEST_METHOD(CheckIfPieceIsAddedOnTile)
		{
			Board::Position position;
			Player player;
			Board board;
			Piece piece;
			std::string nameOfTile = "s";
			Tile startTile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			Tile playerTile(Tile::Side::City, Tile::Side::Road, Tile::Side::Road, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::Intersection);

			piece.setPieceZone(Piece::PieceZone::Bottom);
			auto startTileWithName = std::make_pair(startTile, nameOfTile);
			auto playerTileWithName = std::make_pair(playerTile, nameOfTile);
			position.first = 71;
			position.second = 72;
			board.setStartTile(std::move(startTileWithName));
			player.addTileOnBoard(std::move(playerTileWithName), position, board);
			player.addPieceOnTile(piece, board[position].value(), piece.getPieceZone(), position);

			Assert::IsTrue(board[position].value().first.getPiece().getPieceZone() == piece.getPieceZone());
		}

		TEST_METHOD(CheckIfEqualOperatorWorksAsIntended)
		{
			Player playerOne;
			Player playerTwo;

			//Must not be equal
			playerTwo.setPlayerName("NotEqual");
			playerOne.setPlayerName("TotalyNotEqual");

			Assert::IsFalse(playerOne == playerTwo);

			//must be equal
			playerTwo.setPlayerName("Equal");
			playerOne.setPlayerName("Equal");

			Assert::IsTrue(playerOne == playerTwo);
		}

		TEST_METHOD(CheckIfAssignCopyOperatorWorksAsIntended)
		{
			Player playerOne;
			playerOne.setPlayerName("TestPlayer");
			Player playerTwo;
			playerTwo = playerOne;

			Assert::IsTrue(playerOne == playerTwo);
		}
	};
}