#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h" 
#include "Tile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(CheckBoardBracketOperator)
		{
			Board board;
			Board::Position position;
			auto&[index1, index2] = position;
			if (board[position].has_value())
				Assert::Fail();
		}

		TEST_METHOD(CheckMethodForSettingStartTile)
		{
			Board::Position position;
			position.first = 72;
			position.second = 72;
			Board board;
			Tile startTile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			std::string nameOfTile = "s";

			auto startTileWithName = std::make_pair(startTile, nameOfTile);
			board.setStartTile(std::move(startTileWithName));

			Assert::IsTrue(board[position].has_value() != false);
			Assert::IsTrue(board[position].value().first.getTop() == startTile.getTop());
			Assert::IsTrue(board[position].value().first.getRight() == startTile.getRight());
			Assert::IsTrue(board[position].value().first.getBottom() == startTile.getBottom());
			Assert::IsTrue(board[position].value().first.getLeft() == startTile.getLeft());
		}

	};
}