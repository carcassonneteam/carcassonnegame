#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h" 
#include "Tile.h"
#include "PlaceTile.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CarcassonneTests
{
	TEST_CLASS(PlacedPieceTests)
	{
	public:

		TEST_METHOD(CheckTowZonesWithRoadMethod)
		{
			Tile tile(Tile::Side::City, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Road, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Side::Filed, Tile::Center::None);
			Assert::IsTrue(PlaceTile::areTwoZonesWithRoad(tile));
		}

		
	};
}